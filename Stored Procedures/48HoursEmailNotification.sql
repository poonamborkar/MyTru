/****** Object:  StoredProcedure [dbo].[48HoursEmailNotificaion]    Script Date: 05-07-2018 19:01:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[48HoursEmailNotificaion]
AS
BEGIN
	Select distinct
		R.IssueId,
		UM.UserId,
		UM.FirstName + ' ' + UM.LastName as [Name],
		UM.MobileNo,
		(Select UserId from UserMaster Where EntityId = Emp.ReportingTo And UserType = 'Employee') as [cc]
	From 
		Customer_ServiceRequest As R inner join 
		UserMaster as UM
	On
		R.EmployeeId=UM.EntityId inner join
		Employee as Emp
	On	
		Emp.EmployeeId = UM.EntityId
	Where
		UM.UserType ='Employee' 
	and 
		R.CreatedOn <= DATEADD(HOUR, -48, GETDATE()) and R.IssueStatus='new'

END
/*select 
		Distinct
		R.IssueId,
		UM.EntityId as entityId,
		R.EmployeeId as EmpId,
		UM.UserId,
		UM.FirstName + ' ' + UM.LastName as [Name]
		from UserMaster As UM,Customer_ServiceRequest As R
		where UM.UserType='Employee' and R.EmployeeId=UM.EntityId	
		And R.CreatedOn <= DATEADD(HOUR, -48, GETDATE()) and R.IssueStatus='new'*/
		


GO

