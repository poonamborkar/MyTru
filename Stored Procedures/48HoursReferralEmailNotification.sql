/****** Object:  StoredProcedure [dbo].[48HoursReferralEmailNotification]    Script Date: 05-07-2018 19:51:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[48HoursReferralEmailNotification]
AS
BEGIN

	Select 
		RS.ReferralId,
		R.FirstName+' '+R.LastName AS ReferName,
		E.FirstName+' '+E.LastName AS [Name],
		RS.ReferralStatus,
		E.Email As UserId,
		UM.UserId AS [cc]
	from 
		Referral as R, 
		ReferralStatus as RS,
		Employee As E,
		UserMaster As UM
	Where
		R.ReferralId = RS.ReferralId
	AND 
		RS.SalesAgentAssinged=E.EmployeeId
	AND 
		RS.ReferralStatus='test' and RS.CreatedOn <= DATEADD(HOUR, -48, GETDATE())
	AND
		E.ReportingTo=UM.EntityId

END

GO

