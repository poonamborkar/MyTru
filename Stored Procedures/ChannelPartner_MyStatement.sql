/****** Object:  StoredProcedure [dbo].[ChannelPartner_MyStatement]    Script Date: 05-07-2018 19:03:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ChannelPartner_MyStatement]
(
	@EntityId int,
	@UserType varchar(20)
)
as      
BEGIN
  SELECT  Distinct                                     
       OrderId,
	   OrderStatus,
       CustomerId,
       (Select FirstName+' '+LastName from Customer where CustomerId = CO.CustomerId)
       as CustomerName,                                                     --get First name and last name from Customer Table
       BookingDate,
       (Select ProjectName From ProjectMaster where ProjectId=CO.ProjectId) 
       as ProjectName,                                                      --get Project Name from ProjectMaster table
       BuildingType+ '-' +FlatNo as FlatNo,
       TotalValue,
       IncentiveProposed,
       IncentiveValue           
  from 
      CustomerOrder CO (nolock) 
  where
      BrokerId=@EntityId
End                           
                                      
GO

