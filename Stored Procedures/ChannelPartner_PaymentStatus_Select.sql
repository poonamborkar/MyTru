/****** Object:  StoredProcedure [dbo].[ChannelPartner_PaymentStatus_Select]    Script Date: 05-07-2018 19:03:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChannelPartner_PaymentStatus_Select]
(
@EntityId int,
@UserType varchar(20) 
)
AS
BEGIN
	Select 
		PS.OrderId,
		CO.IncentiveValue as BrokerageAmount,
		PS.InVoiceNo,
		PS.InvoiceDate,
		PS.PaymentMade,
		PS.AmountCr,
		PS.AmountDr,
		PS.ChequeNo,
		PS.DateOfCheque
	FROM
		ChannelPartnerMaster AS CP
	INNER JOIN
		CustomerOrder AS CO
	ON
		CO.BrokerId=CP.BrokerId
	INNER JOIN
		ChannelPartnerPaymentStatus AS PS
	ON
		CO.OrderId=PS.OrderId
	where
		CP.BrokerId=@EntityId
	AND
		CP.IsActive = 'True'

	--	CustomerOrder AS CO
	--WHERE
	--	CO.BrokerId=CP.BrokerId
	--AND
	--	CO.OrderId=PS.OrderId
	--AND
	--	CP.BrokerId=@EntityId
	--AND
		
END
GO

