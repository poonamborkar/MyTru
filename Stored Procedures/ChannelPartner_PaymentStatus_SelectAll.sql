/****** Object:  StoredProcedure [dbo].[ChannelPartner_PaymentStatus_SelectAll]    Script Date: 05-07-2018 19:04:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChannelPartner_PaymentStatus_SelectAll]
AS
BEGIN
	Select 
		OrderId,
		BrokerageAmount,
		InVoiceNo,
		InvoiceDate,
		PaymentMade,
		AmountCr,
		AmountDr,
		ChequeNo,
		DateOfCheque
	FROM
		ChannelPartnerPaymentStatus
	WHERE
		IsActive = 'True'
END
GO

