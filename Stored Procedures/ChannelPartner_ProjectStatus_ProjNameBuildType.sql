/****** Object:  StoredProcedure [dbo].[ChannelPartner_ProjectStatus_ProjNameBuildType]    Script Date: 05-07-2018 19:05:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ChannelPartner_ProjectStatus_ProjNameBuildType] --'PuneVille Phase 1','A'
(
	@ProjectName varchar(20),
	@BuildingType varchar(20)	
)
   AS
   BEGIN
   SELECT
	   Distinct CurrentStatus,
	   EstimationCompletionDates,
	   PlannedActivity
	   --(Select ImageUrl From Tru_Images where ProjectName=@ProjectName And BuildingType=@BuildingType)
   FROM 
	ProjectStatus PS (nolock) 
	INNER JOIN
	ProjectMaster PM (nolock)
   ON
	PS.ProjectId = PM.ProjectId	
   Where 
	PM.ProjectName=@ProjectName
   AND 
	PS.BuildingType=@BuildingType

Select ImageUrl From Tru_Images where ProjectName=@ProjectName And BuildingType=@BuildingType
end


GO

