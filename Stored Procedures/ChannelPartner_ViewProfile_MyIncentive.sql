/****** Object:  StoredProcedure [dbo].[ChannelPartner_ViewProfile_MyIncentive]    Script Date: 05-07-2018 19:05:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ChannelPartner_ViewProfile_MyIncentive] --21,'ChannelPartner'
(
	@EntityId Int,
	@UserType Varchar(30)
)
AS
BEGIN
	SELECT Sum (Round (CO.IncentiveValue,0,0)) AS MyIncentive
	From 
		CustomerOrder As CO
	Where
		CO.BrokerId=@EntityId 
END


GO

