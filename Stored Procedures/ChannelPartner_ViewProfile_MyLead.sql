/****** Object:  StoredProcedure [dbo].[ChannelPartner_ViewProfile_MyLead]    Script Date: 05-07-2018 19:06:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ChannelPartner_ViewProfile_MyLead] --22,'ChannelPartner'
(
	@EntityId Int,
	@UserType Varchar(30)
)
AS
BEGIN
	SELECT COUNT (RS.ReferralId) As MyLeads
	From 
		ReferralStatus As RS,
		ChannelPartnerMaster As CP
	Where
		RS.BrokerAssinged=@EntityId 
	AND RS.BrokerAssinged=CP.BrokerId
END

GO

