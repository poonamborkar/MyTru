/****** Object:  StoredProcedure [dbo].[ChannelPartner_ViewProfile_MyOrders]    Script Date: 05-07-2018 19:06:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ChannelPartner_ViewProfile_MyOrders] --22,'ChannelPartner'
(
	@EntityId Int,
	@UserType Varchar(30)
)
AS
BEGIN
	SELECT COUNT (CO.OrderId) As MyOrders
	From 
		CustomerOrder As CO,
		ChannelPartnerMaster As CP
		
	Where
		CO.BrokerId=@EntityId AND
		@UserType='ChannelPartner'
END

GO

