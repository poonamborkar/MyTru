/****** Object:  StoredProcedure [dbo].[ChannelPartner_ViewProfile_Select]    Script Date: 05-07-2018 19:07:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChannelPartner_ViewProfile_Select]
(
	@EntityId INT,
	@UserType VARCHAR(50)
)
AS
BEGIN
	SELECT
		CP.BrokerId ,
		(CP.FirstName+' '+ CP.LastName) AS Name,
		CP.ReraNo,
		CP.Organization,
		CP.PanNo,
		CP.AadharNo,
		CP.[Type],
		CP.AddressLine1, 
		CP.AddressLine2,
		CP.Landmark, 
		CP.Area,
		CP.City,
		CP.[State] ,
		CP.Country,
		CP.Pincode,
		CP.MobileNo,
		CP.AlternateNumber,
		CP.Email,		
		CP.CrmAgent 
	FROM
		[dbo].[ChannelPartnerMaster](nolock) as CP
	WHERE
		BrokerId=@EntityId
End
GO

