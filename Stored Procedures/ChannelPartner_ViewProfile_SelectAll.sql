/****** Object:  StoredProcedure [dbo].[ChannelPartner_ViewProfile_SelectAll]    Script Date: 05-07-2018 19:07:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChannelPartner_ViewProfile_SelectAll]
AS
BEGIN
	SELECT
		CP.BrokerId,
		(CP.Title +''+CP.FirstName+' '+ CP.LastName) AS Name,
		CP.ReraNo,
		CP.AddressLine1, 
		CP.AddressLine2,
		CP.Landmark, 
		CP.Area ,
		CP.City,
		CP.[State] ,
		CP.Country,
		CP.Pincode,
		CP.MobileNumber,
		CP.AlternateNumber,
		CP.Email,
		CP.IsActive
	FROM
		[dbo].[ChannelPartner] as CP
	WHERE
	    IsActive=1
END
GO

