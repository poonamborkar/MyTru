/****** Object:  StoredProcedure [dbo].[ChannelPartner_ViewProfile_Update]    Script Date: 05-07-2018 19:07:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChannelPartner_ViewProfile_Update]
(
@BrokerId int,
@UserType varchar(50), 
@AddressLine1 VARCHAR(50), 
@Landmark VARCHAR(50),  
@City VARCHAR(50), 
@State VARCHAR(50),
@Country VARCHAR(50), 
@Pincode VARCHAR(8),  
@AlternateNumber VARCHAR(12), 
@ReraNo varchar(30),
@Organization varchar(50),
@Type varchar(25)
--@Id int

)
AS
BEGIN
IF(@UserType='ChannelPartner')
BEGIN
	UPDATE ChannelPartnerMaster
	SET
		AddressLine1=@AddressLine1, 
		Landmark=@Landmark, 
		City=@City,
		[State]=@State ,
		Country=@Country,
		Pincode=@Pincode,
		AlternateNumber=@AlternateNumber,
		ReraNo = @ReraNo,
		Organization = @Organization,
		Type  = @Type
		--,UpdatedBy = @Id
WHERE BrokerId=@BrokerId
END
--ELSE
--BEGIN 
--	UPDATE Referral
--	SET 
--		Title = @Title,
--		FirstName=@FirstName, 
--		LastName=@LastName, 
--		AddressLine1=@AddressLine1,
--		AddressLine2=@AddressLine2, 
--		City=@City, 
--		[State]=@State, 
--		Country=@Country,
--		Pincode=@Pincode, 
--		MobileNo=@MobileNo, 
--		AlternateNo=@AlternateNumber, 
--		Email=@Email,
--		DOB=@DateOfBirth
--WHERE 
--		ReferralId=@BrokerId 		       
END

GO

