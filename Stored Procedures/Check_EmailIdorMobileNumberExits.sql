/****** Object:  StoredProcedure [dbo].[Check_EmailIdorMobileNumberExits]    Script Date: 05-07-2018 19:08:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Check_EmailIdorMobileNumberExits]
(
	@Search varchar(50)
)
AS
BEGIN     
    SELECT
		MobileNo,
		UserId
	FROM 
		UserMaster 
	WHERE 
		MobileNo=@Search 
	OR 
		UserId=@Search 
END
GO

