/****** Object:  StoredProcedure [dbo].[CustOrderExistOrNot]    Script Date: 05-07-2018 19:26:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [dbo].[CustOrderExistOrNot] --'4554545454','string','PuneVille Phase 1','A','702'

(
    @MobileNo varchar(20),
	@UserId varchar(50),
	@ProjectName varchar(50),
	@BuildingType varchar(20),
	@FlatNo varchar(10)
)
AS
BEGIN
	
		IF EXISTS (select CustomerId from CustomerOrder where CustomerId in(select EntityId from UserMaster where MobileNo=@MobileNo and UserId=@UserId) and ProjectId=(select ProjectId from ProjectMaster where ProjectName=@ProjectName) and BuildingType=@BuildingType and FlatNo=@FlatNo)
		select 'Success'
		else
		select 'Not Found'
END
GO

