/****** Object:  StoredProcedure [dbo].[CustomerCreateOrder_BasedOnProjectDetails]    Script Date: 05-07-2018 19:23:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CustomerCreateOrder_BasedOnProjectDetails]
(
@CustomerId int,
@JointName1 varchar(20),
@JointName2 varchar(25),
@ProjectId int,
@BuildingType varchar(20),
@ProjectType varchar(20),
@FlatType varchar(15),
@FlatNumber varchar(10),
@SalesAgentId int,
@EmployeeId int,
@BrokerId int,
@AgreementStatus varchar(15),
@AgreedCost bigint,
@TotalValue bigint,
@BookingAmount bigint,
@Remarks varchar(200),
@CreatedBy varchar(25),
@UpdatedBy varchar(25),
@BookingDate date,
@AgreementDate date,
@BookingDay varchar(20),
@Scheme varchar(20),
@Rate bigint,
@TotalDue bigint,
@RegistrationNo varchar(25),
@Source varchar(25),
@SourceDetails varchar(50),
@Bank varchar(25),
@PANNo1 varchar(25),
@PANNo2 varchar(25),
@PANNo3 varchar(25),
@StampDuty float, 
@RegistrationTax float,
@TDS float, 
@GST float,
@Parking float, 
@FloorRise float, 
@PLC float, 
@InfraCharge float,
@CorrespondenceAddress varchar(30)
)
AS
--declare @Available varchar(10);
 --Set @Available=(Select Available from ProjectDetails as PD where @FlatNumber=PD.FlatNo);
BEGIN
if ((Select Available from ProjectDetails as PD where @FlatNumber=PD.FlatNo)='Yes')
BEGIN
INSERT INTO CustomerOrder
(
CustomerId,
JointName1,
JointName2,
ProjectId,
BuildingType,
ProjectType,
FlatType,
FlatNumber,
SalesAgentId,
EmployeeId,
BrokerId,
AgreementStatus,
AgreedCost,
TotalValue,
BookingAmount,
Remarks,
CreatedBy,
CreatedOn,
UpdatedBy,
UpdatedOn,
BookingDate,
AgreementDate,
BookingDay,
Scheme,
Rate,
TotalDue,
RegistrationNo,
Source,
SourceDetails,
Bank,
PANNo1,
PANNo2,
PANNo3,
IsActive,
StampDuty, 
RegistrationTax ,
TDS , 
GST,
Parking, 
FloorRise, 
PLC, 
InfraCharge,
CorrespondenceAddress 
)
VALUES
(
@CustomerId,
@JointName1,
@JointName2,
@ProjectId ,
@BuildingType,
@ProjectType,
@FlatType,
@FlatNumber,
@SalesAgentId,
@EmployeeId,
@BrokerId,
@AgreementStatus,
@AgreedCost,
@TotalValue,
@BookingAmount,
@Remarks,
@CreatedBy,
GETDATE(),
@UpdatedBy,
GETDATE(),
@BookingDate,
@AgreementDate,
@BookingDay,
@Scheme,
@Rate,
@TotalDue,
@RegistrationNo,
@Source,
@SourceDetails,
@Bank,
@PANNo1,
@PANNo2,
@PANNo3,
'True',
@StampDuty, 
@RegistrationTax ,
@TDS , 
@GST,
@Parking, 
@FloorRise, 
@PLC, 
@InfraCharge,
@CorrespondenceAddress 
)
Update Projectdetails SET Available = 'NO'  where FlatNo = @FlatNumber
end
else
Begin 
Print ('Flat Not Available')
end
END
GO

