/****** Object:  StoredProcedure [dbo].[CustomerOrder_AgreementStatus_Update]    Script Date: 05-07-2018 19:24:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CustomerOrder_AgreementStatus_Update]
(
	@OrderId int,
	@AgreementStatus varchar(20),
	@BookingDay varchar(20),
	@AgreementDate Date,
	@RegistrationNo varchar(25),
	@CorrespondenceAddress varchar(30),
	@Remarks varchar(200)
	--@Id int
)
AS
BEGIN
	Update CustomerOrder 
	SET
		AgreementStatus = @AgreementStatus,
		BookingDay = @BookingDay,
		AgreementDate = @AgreementDate,
		RegistrationNo = @RegistrationNo,
		CorrespondenceAddress = @CorrespondenceAddress,
		Remarks = @Remarks
		--,UpdatedBy = @Id
	where 
		OrderId = @OrderId
END
GO

