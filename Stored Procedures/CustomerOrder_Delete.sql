/****** Object:  StoredProcedure [dbo].[CustomerOrder_Delete]    Script Date: 05-07-2018 19:24:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CustomerOrder_Delete]
(
@MobileNo varchar(15),
@Email varchar(20)
)
AS
BEGIN

Update CustomerOrder
SET
IsActive = 'False'

FROM
Customer AS C,
CustomerOrder AS CO

WHERE

CO.IsActive = 'true'
AND
C.CustomerId= CO.CustomerId
AND
C.MobileNo = @MobileNo
AND
C.Email = @Email

END
GO

