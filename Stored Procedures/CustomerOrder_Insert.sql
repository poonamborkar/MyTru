/****** Object:  StoredProcedure [dbo].[CustomerOrder_Insert]    Script Date: 05-07-2018 19:25:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CustomerOrder_Insert]
(

	@Id int,
	@UserType varchar(20),
	@JointName1 varchar(20)= NULL,
	@JointName2 varchar(25)= NULL,
	@ProjectName varchar(30),
	@BuildingType varchar(30),
	@ProjectType varchar(30),
	@LayoutType varchar(10),
	@BHK varchar(15),
	@FlatNo varchar(10),
	@SalesAgentId int = NULL,
	@EmployeeId int,
	@BrokerId int = NULL,
	@AgreedCost bigint,
	@TotalValue bigint,
	@BookingAmount bigint,
	@BookingDate date,
	@Rate bigint,
	@TotalDue bigint,
	@Source varchar(25),
	@SourceDetails varchar(50) = NULL,
	@BankName varchar(25) = NULL,
	@PANNo1 varchar(25),
	@PANNo2 varchar(25)=NULL,
	@PANNo3 varchar(25)=NULL,
	@StampDuty float   , 
	@RegistrationTax float ,
	@TDS float, 
	@GST float ,
	@ParkingCharge float, 
	@FloorRise float, 
	@PLC float, 
	@InfraCharge float,
	@MaintenanceDeposit float,
	@IncentiveProposed float,
	@IncentiveValue float,
	@AadharCard varchar(30),
	@PaymentMilestone varchar(30) = NULL,
	@CompanyName varchar(30) = NULL, 
	@Designation varchar(30) = NULL, 
	@BankExecutiveName varchar(30) = NULL,
	@LoanSanctionLetter varchar(20) = NULL ,
	@LoanSanctionDate date = NULL, 
	@NOCIssued varchar(20) = NULL, 
	@NOCIssuedDate date = NULL,
	@DiscountSubTotal float ,
	@Tax float,
	@OrderStatus varchar(30),
	@DiscountScheme varchar(30) 
)
AS
	--Auto Generate Password
	declare @AutoGenPswd varchar(10);
	SET @AutoGenPswd=(SELECT SUBSTRING(CONVERT(varchar(40), NEWID()),1,6)); 
	--Auto select BackOffice Agent Data
	declare @BackOfficeAgentName varchar(50);
	declare @BackOfficeAgentEmail varchar(50);
	select top 1 @BackOfficeAgentName=FirstName+' '+LastName ,@BackOfficeAgentEmail=Email from Employee nolock where Role like '%BackOffice%' ORDER BY NEWID()
	--To avoid date bound exception in Sql set null when there is no data in date
	declare @BookingDate1 DATE;  
	if(@BookingDate='1/1/1753 12:00:00 AM')
		SET @BookingDate1=NULL
	Else 
		SET @BookingDate=@BookingDate
	--set Default value to DiscountScheme
	DECLARE @DiscountScheme1 float;
	If(@DiscountScheme = 'No Scheme')
		Set @DiscountScheme1 = 1
	else
		set @DiscountScheme1 = @DiscountScheme
	
	--set Customer Or Referral Based On search output
	declare @CustomerId int;
	declare @ReferralId int;
	if(@UserType='Customer')
		Set @CustomerId=@Id
	else
		set @CustomerId=null

	if(@UserType='Referral')
		Set @ReferralId=@Id
	else
		set @ReferralId=null
BEGIN
   BEGIN TRY
	  BEGIN TRAN
	  --insert into CustomerOrder table
		INSERT INTO CustomerOrder
		(	CustomerId,
			JointName1,
			JointName2,
			ProjectId,
			BuildingType,
			LayoutType,
			ProjectType,
			BHK,
			FlatNo,
			SalesAgentId,
			EmployeeId,
			BrokerId,
			AgreedCost,
			TotalValue,
			BookingAmount,
			--CreatedOn,
			BookingDate,
			Rate,
			TotalDue,
			Source,
			SourceDetails,
			BankName,
			PANNo1,
			PANNo2,
			PANNo3,
			IsActive,
			StampDuty, 
			RegistrationTax ,
			TDS , 
			GST,
			ParkingCharge, 
			FloorRise, 
			PLC, 
			InfraCharge,
			MaintenanceDeposit,
			IncentiveProposed,
			IncentiveValue,
			AadharCard,
			PaymentMilestone,
			CompanyName, 
			Designation, 
			BankExecutiveName,
			LoanSanctionLetter,
			LoanSanctionDate, 
			NOCIssued, 
			NOCIssuedDate,
			DiscountSubTotal,
			Tax,
			OrderStatus,
			DiscountScheme,
			ReferralId,
			CreatedBy
		)
		VALUES
		(
			@CustomerId,
			@JointName1,
			@JointName2,
			(Select ProjectId from ProjectMaster Where ProjectName = @ProjectName),
			@BuildingType,
			@LayoutType,
			@ProjectType,
			@BHK,
			@FlatNo,
			@SalesAgentId,
			@EmployeeId,
			@BrokerId,
			@AgreedCost,
			@TotalValue,
			@BookingAmount,
			--GETDATE(),
			@BookingDate,
			@Rate,
			@TotalDue,
			@Source,
			@SourceDetails,
			@BankName,
			@PANNo1,
			@PANNo2,
			@PANNo3,
			'True',
			@StampDuty, 
			@RegistrationTax ,
			@TDS , 
			@GST,
			@ParkingCharge, 
			@FloorRise, 
			@PLC, 
			@InfraCharge,
			@MaintenanceDeposit,
			@IncentiveProposed,
			@IncentiveValue,
			@AadharCard,
			@PaymentMilestone,
			@CompanyName, 
			@Designation, 
			@BankExecutiveName,
			@LoanSanctionLetter,
			@LoanSanctionDate, 
			@NOCIssued, 
			@NOCIssuedDate,
			@DiscountSubTotal,
			@Tax,
			@OrderStatus,
			@DiscountScheme1,
			@ReferralId,
			(Select Id from UserMaster where EntityId=@EmployeeId and UserType = @UserType)
		)
--------------------------------------------------------------------------------------------------------
	   --Update Availability in ProjectDetails table to NotAvailable On Order Submit
		Update ProjectDetails Set Available='No' where ProjectId=(Select ProjectId from ProjectMaster nolock where ProjectName=@ProjectName) and BuildingType=@BuildingType and BHK=@Bhk and FlatNo=@FlatNo and LayoutType=@LayoutType
--------------------------------------------------------------------------------------------------------
		if (@UserType = 'Referral')
		Begin
			--On Order Submit by Referral,insert into Customer Table to make Refferral as Customer
			Insert Into Customer
			(	
				Title, FirstName, LastName, MobileNo, Email, AddressLine1, AddressLine2, Landmark, Area,City, State, Country,  
				Pincode, Occupation, AlternateNo, IsActive, Dob, CreatedOn, CreatedBy, UpdatedBy, UpdatedOn, ReferralId
			)
			values
			(
				(select Title from Referral nolock where ReferralId=@ReferralId),
				(select FirstName from Referral nolock where ReferralId=@ReferralId),
				(select LastName from Referral nolock where ReferralId=@ReferralId),
				(select MobileNo from Referral nolock where ReferralId=@ReferralId),
				(select Email from Referral nolock where ReferralId=@ReferralId),
				(select AddressLine1 from Referral nolock where ReferralId=@ReferralId),
				(select AddressLine2 from Referral nolock where ReferralId=@ReferralId),
				(select Landmark from Referral nolock where ReferralId=@ReferralId),
				(select Area from Referral nolock where ReferralId=@ReferralId),
				(select City from Referral nolock where ReferralId=@ReferralId),
				(select State from Referral nolock where ReferralId=@ReferralId),
				(select Country from Referral nolock where ReferralId=@ReferralId),
				(select Pincode from Referral nolock where ReferralId=@ReferralId),
				(select Occupation from Referral nolock where ReferralId=@ReferralId),
				(select AlternateNo from Referral nolock where ReferralId=@ReferralId),
				(select IsActive from Referral nolock where ReferralId=@ReferralId),
				(select Dob from Referral nolock where ReferralId=@ReferralId),
				(select CreatedOn from Referral nolock where ReferralId=@ReferralId),
				(select CreatedBy from Referral nolock where ReferralId=@ReferralId),
				(select UpdatedBy from Referral nolock where ReferralId=@ReferralId),
				(select UpdatedOn from Referral nolock where ReferralId=@ReferralId),
				@ReferralId
			)		
			--------------------------------------------------------------------------------------------------------
			--Update CustomerId into CustomerOrder Table when Refferral turns into Customer on his order submit
			Update CustomerOrder Set CustomerId=(select top 1 CustomerId from Customer nolock where ReferralId=@ReferralId) where ReferralId=@ReferralId
			--------------------------------------------------------------------------------------------------------
			--Insert Or Update UserMaster Table on Referral Entry to Customer when Refferral turns into Customer on his order submit
			If EXISTS (select EntityId from UserMaster nolock where EntityId=@ReferralId and UserType='Referral')
				Begin
					Update UserMaster Set EntityId=(select top 1 CustomerId from Customer nolock where ReferralId=@ReferralId), UserType='Customer' where EntityId=@ReferralId and UserType='Referral'
					
					select 
						@ReferralId as CustomerId,
						(Select FirstName+' '+LastName from Referral nolock where ReferralId=@ReferralId) as CustomerName,
						(Select Email From Referral nolock where ReferralId=@ReferralId) AS [CustomerEmailId],
						(select UserId from UserMaster nolock where EntityId=(select ReferredById from Referral where ReferralId=@ReferralId) and  UserType=(select ReferredByType from Referral where ReferralId=@ReferralId))as ReferredByEmailId,
						(select FirstName+' '+LastName from UserMaster nolock where EntityId=(select ReferredById from Referral where ReferralId=@ReferralId) and UserType=(select ReferredByType from Referral where ReferralId=@ReferralId))as ReferredByName,
						@BackOfficeAgentEmail as BackOfficeAgentEmailId,
						@BackOfficeAgentName as BackOfficeAgentName,
						(select UserId from UserMaster nolock where EntityId=@EmployeeId and UserType='Employee') as EmpEmailId,
						(select FirstName+' '+LastName from UserMaster nolock where EntityId=@EmployeeId and UserType='Employee') as EmpName
						--@AutoGenPswd as [Password]
					from 
						Referral as R inner join
						CustomerOrder as CO 
					ON
						R.ReferralId=CO.ReferralId 
					Where CO.OrderId=(select top 1 OrderId from CustomerOrder nolock where ReferralId=@ReferralId ORDER BY CO.OrderId DESC)

				End
			Else
				Begin
					Insert into UserMaster
					(
						FirstName, LastName, UserId, EncryptPass, pwd, UserType, MobileNo, IsActive, EntityId, CreatedOn
					)
					values
					(
						(select FirstName from Referral nolock where ReferralId=@ReferralId),
						(select LastName from Referral nolock where ReferralId=@ReferralId),
						(select Email from Referral nolock where ReferralId=@ReferralId),
						EncryptByPassPhrase('VovTru',CONVERT(varbinary(500),@AutoGenPswd)),
						@AutoGenPswd,
						'Customer',
						(select MobileNo from Referral nolock where ReferralId=@ReferralId),
						(select IsActive from Referral nolock where ReferralId=@ReferralId),
						(select top 1 CustomerId from Customer nolock where ReferralId=@ReferralId),
						(select CreatedOn from Referral nolock where ReferralId=@ReferralId)
					)
					--When Order is Booked,Send Emails to person whose order is submited and also cc to person who referred him
					select 
						@ReferralId as CustomerId,
						(Select FirstName+' '+LastName from Referral nolock where ReferralId=@ReferralId) as CustomerName,
						(Select Email From Referral nolock where ReferralId=@ReferralId) AS [CustomerEmailId],
						(select UserId from UserMaster nolock where EntityId=(select ReferredById from Referral where ReferralId=@ReferralId) and  UserType=(select ReferredByType from Referral where ReferralId=@ReferralId))as ReferredByEmailId,
						(select FirstName+' '+LastName from UserMaster nolock where EntityId=(select ReferredById from Referral where ReferralId=@ReferralId) and UserType=(select ReferredByType from Referral where ReferralId=@ReferralId))as ReferredByName,
						@BackOfficeAgentEmail as BackOfficeAgentEmailId,
						@BackOfficeAgentName as BackOfficeAgentName,
						@AutoGenPswd as [Password],
						(select UserId from UserMaster nolock where EntityId=@EmployeeId and UserType='Employee') as EmpEmailId,
						(select FirstName+' '+LastName from UserMaster nolock where EntityId=@EmployeeId and UserType='Employee') as EmpName
					from 
						Referral as R inner join
						CustomerOrder as CO 
					ON
						R.ReferralId=CO.ReferralId 
					Where CO.OrderId=(select top 1 OrderId from CustomerOrder nolock where ReferralId=@ReferralId ORDER BY CO.OrderId DESC)
				
				End
		
		End 
		Else
			begin				
				--When Order is Booked,Send Emails to person whose order is submited and also cc to person who referred him
				select 
					@Id as CustomerId,
					(Select FirstName+' '+LastName from Customer nolock where CustomerId=@Id) as CustomerName,
					(Select Email From Customer nolock where CustomerId=@Id) AS [CustomerEmailId],
					(select UserId from UserMaster nolock where EntityId=(select ReferredById from Referral where ReferralId=@ReferralId) and  UserType=(select ReferredByType from Referral where ReferralId=@ReferralId))as ReferredByEmailId,
					(select FirstName+' '+LastName from UserMaster nolock where EntityId=(select ReferredById from Referral where ReferralId=@ReferralId) and UserType=(select ReferredByType from Referral where ReferralId=@ReferralId))as ReferredByName,
					@BackOfficeAgentEmail as BackOfficeAgentEmailId,
					@BackOfficeAgentName as BackOfficeAgentName,
					(select UserId from UserMaster nolock where EntityId=@EmployeeId and UserType= 'Employee') as EmpEmailId,
					(select FirstName+' '+LastName from UserMaster nolock where EntityId=@EmployeeId and UserType='Employee') as EmpName
				from 
					Customer as C inner join
					CustomerOrder as CO 
				ON
					C.CustomerId=CO.CustomerId 
				Where CO.OrderId=(select top 1 OrderId from CustomerOrder nolock where CustomerId=@Id ORDER BY CO.OrderId DESC)	
			
				print 'Your Order is Submited...'
				print @@ERROR
			end
------------------------------------------------------
--check if all queries successfully perfom then only Commit else re-execute(rollback) queries
	COMMIT TRAN
	print'Transaction is Commited'
   END TRY
  BEGIN CATCH
	IF @@TRANCOUNT > 0	
		ROLLBACK TRAN
		print'Transaction rolled back'
  END CATCH
END
GO

