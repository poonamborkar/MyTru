/****** Object:  StoredProcedure [dbo].[CustomerOrder_Order_Select]    Script Date: 05-07-2018 19:25:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CustomerOrder_Order_Select] 
(
	@EntityId int,
	@UserType Varchar(30)=NULL,
	@CustomerId int = NULL,
	@ProjectName varchar(30) = NULL,
	@BuildingType varchar(30) = NULL,
	@BookingDateFrom Date = NULL,
	@BookingDateTo Date = NULL,
	@Id int=0
)
AS
BEGIN
	IF @Id=0
	BEGIN	
	SELECT 
		CO.OrderId,
		CO.CustomerId,
		(C.FirstName + ' ' + C.LastName) AS CustomerName,
		C.MobileNo,
		C.Email,
		CO.JointName1,
		CO.JointName2,
		CO.ProjectId,
		PM.ProjectName,
		CO.BuildingType,
		CO.ProjectType,
		CO.LayoutType,
		CO.BHK,
		CO.FlatNo,
		CO.SalesAgentId,
		CO.EmployeeId,
		CO.BrokerId,
		CO.AgreementStatus,
		CO.AgreedCost,
		CO.TotalValue,
		CO.BookingAmount,
		CO.Remarks,
		CO.CreatedBy,
		CO.UpdatedBy,
		CO.BookingDate,
		CO.AgreementDate,
		CO.BookingDay,
		CO.DiscountScheme,
		CO.Rate,
		CO.TotalDue,
		CO.RegistrationNo,
		CO.Source,
		CO.SourceDetails,
		CO.BankName,
		CO.PANNo1,
		CO.PANNo2,
		CO.PANNo3,
		CO.StampDuty,
		CO.RegistrationTax,
		CO.TDS,
		CO.GST,
		CO.ParkingCharge,
		CO.FloorRise,
		CO.PLC,
		CO.InfraCharge,
		CO.CorrespondenceAddress,
		PD.CarpetArea,
		PD.ReraArea,
		PD.AttachedTerrace,
		PD.EnclosedBalcony,
		PD.ParkingSlot,
		CO.MaintenanceDeposit
	FROM
		CustomerOrder AS CO,
		Customer AS C,
		ProjectMaster AS PM,
		ProjectDetails AS PD,
		Employee AS E

	WHERE
		CO.ProjectId = PM.ProjectId
	AND
		(PM.ProjectName = @ProjectName OR @ProjectName IS NULL)
	AND 
		C.CustomerId = CO.CustomerId
	AND 
		(C.CustomerId = @CustomerId OR @CustomerId IS NULL)
	AND 
		CO.BuildingType = PD.BuildingType
	AND 
		(PD.BuildingType = @BuildingType OR @BuildingType IS NULL)
	AND 
		((CO.BookingDate BETWEEN @BookingDateFrom AND @BookingDateTo) 
			OR 
		( @BookingDateFrom IS NULL AND @BookingDateTo IS NULL))
	AND 
		PM.ProjectId = PD.ProjectId
	AND 
		CO.FlatNo=PD.FlatNo
	AND 
		(CO.EmployeeId = E.EmployeeId OR CO.SalesAgentId= E.EmployeeId)
	AND 
		(E.EmployeeId in (Select EmployeeId from Employee where ReportingTo = @EntityId)
	OR 
		E.EmployeeId in (Select EmployeeId from Employee where ReportingTo in (Select EmployeeId from Employee where ReportingTo = @EntityId))
	OR
		E.EmployeeId=@EntityId)
	END

	Else
	BEGIN
	SELECT 
		CO.OrderId,
		CO.CustomerId,
		(C.FirstName + ' ' + C.LastName) AS CustomerName,
		C.MobileNo,
		C.Email,
		CO.JointName1,
		CO.JointName2,
		CO.ProjectId,
		PM.ProjectName,
		CO.BuildingType,
		CO.ProjectType,
		CO.LayoutType,
		CO.BHK,
		CO.FlatNo,
		CO.SalesAgentId,
		CO.EmployeeId,
		CO.BrokerId,
		CO.AgreementStatus,
		CO.AgreedCost,
		CO.TotalValue,
		CO.BookingAmount,
		CO.Remarks,
		CO.CreatedBy,
		CO.UpdatedBy,
		CO.BookingDate,
		CO.AgreementDate,
		CO.BookingDay,
		CO.DiscountScheme,
		CO.Rate,
		CO.TotalDue,
		CO.RegistrationNo,
		CO.Source,
		CO.SourceDetails,
		CO.BankName,
		CO.PANNo1,
		CO.PANNo2,
		CO.PANNo3,
		CO.StampDuty,
		CO.RegistrationTax,
		CO.TDS,
		CO.GST,
		CO.ParkingCharge,
		CO.FloorRise,
		CO.PLC,
		CO.InfraCharge,
		CO.CorrespondenceAddress,
		PD.CarpetArea,
		PD.ReraArea,
		PD.AttachedTerrace,
		PD.EnclosedBalcony,
		PD.ParkingSlot,
		CO.MaintenanceDeposit
	FROM
		CustomerOrder AS CO,
		Customer AS C,
		ProjectMaster AS PM,
		ProjectDetails AS PD,
		Employee AS E

	WHERE
		CO.ProjectId = PM.ProjectId
	AND
		(PM.ProjectName = @ProjectName OR @ProjectName IS NULL)
	AND 
		C.CustomerId = CO.CustomerId
	AND 
		(C.CustomerId = @CustomerId OR @CustomerId IS NULL)
	AND 
		CO.BuildingType = PD.BuildingType
	AND 
		(PD.BuildingType = @BuildingType OR @BuildingType IS NULL)
	AND 
		((CO.BookingDate BETWEEN @BookingDateFrom AND @BookingDateTo) 
		OR 
		( @BookingDateFrom IS NULL AND @BookingDateTo IS NULL))
	AND 
		PM.ProjectId = PD.ProjectId
	AND 
		CO.FlatNo=PD.FlatNo
	AND 
		(CO.EmployeeId = E.EmployeeId
		OR 
		CO.SalesAgentId= E.EmployeeId)
	AND 
		E.EmployeeId=@Id
	END
END
GO

