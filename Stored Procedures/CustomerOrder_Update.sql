/****** Object:  StoredProcedure [dbo].[CustomerOrder_Update]    Script Date: 05-07-2018 19:25:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CustomerOrder_Update]
(


@OrderId int,
@CustomerId int,
@JointName1 varchar(20),
@JointName2 varchar(25),
@ProjectName varchar(30),
@BuildingType varchar(20),
@ProjectType varchar(20),
@LayoutType varchar(10),
@BHK varchar(15),
@FlatNumber varchar(10),
@SalesAgentId int,
@EmployeeId int,
@BrokerId int,
@AgreedCost bigint,
@TotalValue bigint,
@BookingAmount bigint,
@BookingDate date,
@Scheme varchar(20),
@Rate bigint,
@TotalDue bigint,
@Source varchar(25),
@SourceDetails varchar(50),
@Bank varchar(25),
@PANNo1 varchar(25),
@PANNo2 varchar(25),
@PANNo3 varchar(25),
@StampDuty float, 
@RegistrationTax float,
@TDS float, 
@GST float,
@Parking float, 
@FloorRise float, 
@PLC float, 
@InfraCharge float,
@MaintenanceDeposit float
)
AS
BEGIN

Update CustomerOrder
SET 
CustomerId=@CustomerId,
JointName1=@JointName1,
JointName2=@JointName2,
ProjectId=(Select ProjectId from ProjectMaster where ProjectName=@ProjectName),
BuildingType=@BuildingType,
ProjectType=@ProjectType,
LayoutType=@LayoutType,
BHK=@BHK,
FlatNo=@FlatNumber,
SalesAgentId=@SalesAgentId,
EmployeeId=@EmployeeId,
BrokerId=@BrokerId,
AgreedCost=@AgreedCost,
TotalValue=@TotalValue,
BookingAmount=@BookingAmount,
CreatedOn=GETDATE(),
UpdatedOn=GETDATE(),
BookingDate=@BookingDate,
--Scheme=@Scheme,
Rate=@Rate,
TotalDue=@TotalValue,
Source=@Source,
SourceDetails=@SourceDetails,
--Bank=@Bank,
PANNo1=@PANNo1,
PANNo2=@PANNo2,
PANNo3=@PANNo3,
StampDuty=@StampDuty,
RegistrationTax=@RegistrationTax,
TDS=@TDS, 
GST=@GST,
--Parking=@Parking, 
FloorRise=@FloorRise, 
PLC = @PLC, 
InfraCharge = @InfraCharge,
MaintenanceDeposit=@MaintenanceDeposit

FROM
Customer AS C,
CustomerOrder AS CO

WHERE
CO.OrderId = @OrderId
And
CO.IsActive = 'true'
AND
C.CustomerId= CO.CustomerId
END
GO

