/****** Object:  StoredProcedure [dbo].[Customer_ApplicationLog_Insert]    Script Date: 05-07-2018 19:09:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ApplicationLog_Insert]
(
@Request varchar(MAX),
@Response varchar(MAX),
@UserId int,
@UserType varchar(50),
@Status bit
)
AS
BEGIN
INSERT INTO 
ApplicationLog
(
Request, 
Response, 
UserId, 
UserType, 
CreatedOn, 
Status
)
VALUES
(
@Request, 
@Response, 
@UserId, 
@UserType, 
GETDATE(), 
@Status
)
END


GO

