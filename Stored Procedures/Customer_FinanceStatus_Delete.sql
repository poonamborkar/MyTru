/****** Object:  StoredProcedure [dbo].[Customer_FinanceStatus_Delete]    Script Date: 05-07-2018 19:10:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Customer_FinanceStatus_Delete]
(
@EntityId int,
	@UserType Varchar(50)
)

AS
BEGIN

	Update  
	FinanceStatus 
	Set 
	IsActive ='false' 
	From
	UserMaster UM
	where 
	CustomerId=@EntityId 
	And
	UM.UserType =@Usertype 



	Update 
	CustomerReceipt 
	Set
	CustomerReceipt.IsActive='False'  
	from  CustomerReceipt CR , 
	CustomerOrder CO,
	UserMaster UM
	where 
	CO.OrderId=CR.OrderId 
	And co.CustomerId=@EntityId AND
	UM.UserType =@Usertype 
END

	

GO

