/****** Object:  StoredProcedure [dbo].[Customer_FinanceStatus_Select]    Script Date: 05-07-2018 19:10:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Customer_ProjectMaster_Select]    Script Date: 5/7/2018 10:13:03 AM ******/
CREATE procedure [dbo].[Customer_FinanceStatus_Select]      
(
	@EntityId int,
	@UserType Varchar(50)
)
as      
--Select By ID For FinanceStatus
Begin      
	select   
		FS.OrderId,
		CONVERT(NVARCHAR,OrderDate,106) As [OrderDates], --Convert Date Format 
		FS.FirstName, 
		FS.LastName, 
		FS.JointName, 
		FS.CustomerId, 
		FS.ReferralName, 
		FS.BrokerId, 
		FS.ProjectType, 
		FS.FlatNumber, 
		CONVERT(NVARCHAR,BanakhatDate,106) As [BanakhatDates], 
		FS.Loan, 
		FS.RegistrationNo, 
		 FS.FinancialStatus,
		CO.BookingAmount,
		CONVERT(NVARCHAR,BookingDate,106) AS [BookingDate]
from 
		FinanceStatus as FS, 
		CustomerOrder as CO, 
		Customer as C
Where 
		FS.IsActive='True' --To Find Table is Deleted or not
		AND CO.CustomerId = @EntityId  AND 
		FS.OrderId = CO.OrderId And
		CO.CustomerId = C.CustomerId
	
	--------------------------------Select By ID For Customer Receipt Table-----------
	select 
		CR.OrderId,
		CR.ReceiptNo,
		CONVERT(NVARCHAR,CR.ReceiptDate,106) As ReceiptDate,
		CR.DocumentNo,
		CR.DocumentType, 
		CR.Total,
		CONVERT(NVARCHAR,CR.ChequeDate,106) As ChequeDate,
		CR.ChequeNumber,
		CR.BankName,
		CR.PayBy,
		CR.ServiceTax,
		CR.PayAgainst,
		CR.Branch,
		CR.Status
	From
		CustomerReceipt AS CR,
		Customer AS C,
		CustomerOrder AS CO,
		FinanceStatus As FS
	WHERE
		CR.OrderId = CO.OrderId And
		CR.OrderId= FS.OrderId And
		CO.CustomerId= @EntityId
		AND CO.CustomerId= C.CustomerId And
		CR.IsActive='TRUE' 
End
GO

