/****** Object:  StoredProcedure [dbo].[Customer_FinanceStatus_SelectAll]    Script Date: 05-07-2018 19:11:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Customer_FinanceStatus_SelectAll]    Script Date: 5/17/2018 3:21:15 PM ******/
/****** Object:  StoredProcedure [dbo].[Customer_ProjectMaster_Select]    Script Date: 5/7/2018 10:13:03 AM ******/
CREATE procedure [dbo].[Customer_FinanceStatus_SelectAll]      
as      
Begin      
--------------------------------Select ALL For Finance Status Table-----------
    select  
		OrderId, 
		CONVERT(NVARCHAR,OrderDate,106) As [OrderDates], --Convert Date Format 
		FirstName, 
		LastName, 
		JointName, 
		CustomerID, 
		ReferralName, 
		BrokerId, 
		ProjectType, 
		FlatNumber,
		CONVERT(NVARCHAR,BanakhatDate,106) As [BanakhatDates],  --Convert Date Format 
		Loan, 
		RegistrationNo, 
		FinancialStatus
	from 
		FinanceStatus AS FS   
		Where FS.IsActive='TRUE'  --To Find Table is Deleted or not

--------------------------------Select ALL For Customer Receipt Table----------
	Select
		CR.OrderId,
		CR.ReceiptNo,
		CONVERT(NVARCHAR,CR.ReceiptDate,106) As ReceiptDate,
		CR.DocumentNo,
		CR.DocumentType, 
		CR.Total,
		CONVERT(NVARCHAR,CR.ChequeDate,106) As ChequeDate,
		CR.ChequeNumber,
		CR.BankName,
		CR.PayBy,
		CR.ServiceTax,
		CR.PayAgainst,
		CR.Branch,
		CR.Status
	FROM 
		CustomerReceipt AS CR
	where 
		CR.IsActive='True'  --To Find Table is Deleted or not
End
GO

