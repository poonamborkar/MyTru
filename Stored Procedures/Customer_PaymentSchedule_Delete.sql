/****** Object:  StoredProcedure [dbo].[Customer_PaymentSchedule_Delete]    Script Date: 05-07-2018 19:11:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Customer_PaymentSchedule_Delete]
(
@EntityId int
)

AS
BEGIN

Update
PaymentSchedule													
SET
IsActive= 'False'					--Updating the IsActive Flag to False				
FROM
PaymentSchedule PS,
CustomerOrder CO
WHERE
CO.OrderId=PS.OrderId
AND
CO.CustomerId=@EntityId
END
GO

