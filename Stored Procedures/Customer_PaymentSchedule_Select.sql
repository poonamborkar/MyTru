/****** Object:  StoredProcedure [dbo].[Customer_PaymentSchedule_Select]    Script Date: 05-07-2018 19:12:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Customer_PaymentSchedule_Select]			
(
	@EntityId int,
	@UserType varchar (50)
)
AS
BEGIN

SELECT									--Displays data by CustomerId from PaymentScedule table
	PS.OrderId, 
	PS.TotalAgreedAmount,
	PS.TotalDemandRaised,
	PS.TotalAmountPaid,
	PS.InstallmentNo,
	PS.PayAgainst,
	PS.[Percentage],
	PS.TotalDue,
	PS.AmountReceived,
	PS.AmountBalance,
	PS.TaxReceived,
	PS.[Status],
	PS.EstimatedBillingDate
FROM 
	PaymentSchedule as PS, 
	CustomerOrder  as CO, Customer as C
WHERE C.CustomerId = @EntityId 
	And 
	CO.CustomerId = C.CustomerId 
	And 
	PS.OrderId = CO.OrderId
	AND
	PS.IsActive= 'True'
END
GO

