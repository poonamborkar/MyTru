/****** Object:  StoredProcedure [dbo].[Customer_PaymentSchedule_SelectAll]    Script Date: 05-07-2018 19:12:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[Customer_PaymentSchedule_SelectAll]			  
as
begin

Select								--Displays all the data from PaymentSchedule table
OrderId, 
TotalAgreedAmount,
TotalDemandRaised,
TotalAmountPaid,
InstallmentNo,
PayAgainst,
[Percentage],
TotalDue,
AmountReceived,
AmountBalance,
TaxReceived,
[Status],
EstimatedBillingDate

from
 PaymentSchedule

 WHERE IsActive = 'True'

end
GO

