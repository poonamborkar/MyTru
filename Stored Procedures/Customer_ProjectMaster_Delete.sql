/****** Object:  StoredProcedure [dbo].[Customer_ProjectMaster_Delete]    Script Date: 05-07-2018 19:12:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Customer_ProjectMaster_Delete]     
(
@ProjectId int,
@UpdatedBy varchar(20)                                                        
)
as
begin
Update                                                    
    ProjectMaster 
Set 
    IsActive='FALSE' 
where 
    ProjectId=@ProjectId 
	and
	UpdatedOn=GETDATE()
	And
	@UpdatedBy=@UpdatedBy   
end
GO

