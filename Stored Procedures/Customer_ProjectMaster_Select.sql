/****** Object:  StoredProcedure [dbo].[Customer_ProjectMaster_Select]    Script Date: 05-07-2018 19:13:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE procedure [dbo].[Customer_ProjectMaster_Select]      

as      

Begin      

    select ProjectId,
	ProjectName,
	ProjectType,
	ReraApproval,
	ProjectImage,
	TotalBuilding,
	NoOfFlat,
	Latitude,
	Longitude,
	Address1,
	Address2,
	Landmark,
	Location,
	City,
	Country,
	PinCode  from ProjectMaster where  IsActive=1

End
GO

