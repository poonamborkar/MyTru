/****** Object:  StoredProcedure [dbo].[Customer_ProjectStatus_Delete]    Script Date: 05-07-2018 19:13:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Customer_ProjectStatus_Delete]     --Create Procedure
(
@ProjectId int
--@UpdatedBy  varchar(20)                                                           
)
as
begin

Update                                                     --Update IsActive to false to show that the record is deleted
    ProjectStatus 
Set 
    IsActive='FALSE' 
where 
    ProjectId=@ProjectId 
   --UpdatedOn=GETDATE() And
   --UpdatedBy=@UpdatedBy          
end
GO

