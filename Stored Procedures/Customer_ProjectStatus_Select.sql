/****** Object:  StoredProcedure [dbo].[Customer_ProjectStatus_Select]    Script Date: 05-07-2018 19:14:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Customer_ProjectStatus_Select]      
(
	@EntityId  int,
	@UserType Varchar(50)
)
as      
If (@UserType = 'customer' )
BEGIN
SELECT DISTINCT                                      
  PM.ProjectName,
  PS.BuildingType,
  PS.CurrentStatus,
  CONVERT(NVARCHAR,EstimationCompletionDates,106) 
  As [EstimationCompletionDate],                       
  PS.PlannedActivity,
  PS.CurrentPrice
 
from ProjectStatus as PS (nolock),CustomerOrder as CO(nolock),
   Customer as C (nolock),
   ProjectMaster as PM                                      
where PS.ProjectId = CO.ProjectId
And CO.CustomerId=@EntityId
And CO.BuildingType=PS.BuildingType
And C.CustomerId = CO.CustomerId
And PS.ProjectId = PM.ProjectId
And PS.IsActive='TRUE' 
End                           
                                      


 


GO

