/****** Object:  StoredProcedure [dbo].[Customer_ProjectStatus_SelectAll]    Script Date: 05-07-2018 19:14:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE procedure [dbo].[Customer_ProjectStatus_SelectAll]                                 --Create Stored Procedure

as      

Begin      

    select                                                                               --Get Attribute for Display
	ProjectId,
	BuildingType,
	CurrentStatus,
	CONVERT(NVARCHAR,EstimationCompletionDates,106) As [EstimationCompletionDate],
	PlannedActivity,
	CurrentPrice,
	IsActive

    from ProjectStatus 
    where IsActive = 1                                                               --Get AllAttribute From Table for Display 

End
GO

