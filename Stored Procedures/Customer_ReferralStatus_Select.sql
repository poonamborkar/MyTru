/****** Object:  StoredProcedure [dbo].[Customer_ReferralStatus_Select]    Script Date: 05-07-2018 19:18:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Customer_ReferralStatus_Select] --21, 'ChannelPartner'
(
	@EntityId int,
	@UserType varchar(20)
)
AS
BEGIN
   
 If(@UserType = 'Customer')
 BEGIN
     SELECT 
		RS.ReferralId,
		RS.ReferralBy,
		--RS.CrmAssinged,
		(Select FirstName + ' '+ LastName from Employee where EmployeeId=RS.CrmAssinged)AS CrmAssinged,
		(Select FirstName +' ' +LastName from ChannelPartnerMaster where BrokerId=RS.BrokerAssinged) AS BrokerAssinged,
		RS.IncentivePraposed,
		(R.FirstName +' '+ R.LastName) AS Name,
		ReferralStatus,
		CONVERT(date, R.CreatedOn) As CreatedOn,
		R.PreferredCity,
		RS.Source,
		RS.Face2Face,
		RS.NoOfSiteVisit,
		RS.SalesAgentAssinged		
	From
		ReferralStatus As RS (nolock) 
	inner join
		Referral As R (nolock)
	--Customer As C	
	On
		RS.ReferralId=R.ReferralId
	Where 
		R.ReferredById=@EntityId
	And 
		RS.IsActive=1
	AND 
		R.ReferredByType=@UserType

	order by
		 ReferralId
END

ELSE IF(@UserType = 'ChannelPartner')
BEGIN
 SELECT 
		RS.ReferralId,
		CP.organization as ReferralBy,
		--RS.CrmAssinged,
		(Select FirstName + ' '+ LastName from Employee where EmployeeId=RS.CrmAssinged)AS CrmAssinged,
		(Select FirstName +' ' +LastName from ChannelPartnerMaster where BrokerId=RS.BrokerAssinged) AS BrokerAssinged,
		RS.IncentivePraposed,
		(R.FirstName +' '+ R.LastName) AS Name,
		ReferralStatus,
		CONVERT(date, R.CreatedOn) As CreatedOn,
		R.PreferredCity,
		RS.Source,
		RS.Face2Face,
		RS.NoOfSiteVisit,
		RS.SalesAgentAssinged
	FROM
		ReferralStatus As RS (nolock)
	Inner Join
		Referral As R (nolock)
	On 
		RS.ReferralId= R.ReferralId 
	Inner Join
		ChannelPartnerMaster AS CP (nolock)
	On
		RS.BrokerAssinged=CP.BrokerId
	
	WHERE
		RS.IsActive !=0 
		AND	   
		CP.BrokerId = @EntityId or CP.organization = (Select Organization from ChannelPartnerMaster where BrokerId = @EntityId)

	order by 
		ReferralId	Desc
END
END

GO

