/****** Object:  StoredProcedure [dbo].[Customer_ReferralStatus_SelectAll]    Script Date: 05-07-2018 19:18:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [dbo].[Customer_ReferralStatus_SelectAll]
AS
BEGIN
    SELECT 
		RS.ReferralId,
		--CONCAT (C.FirstName,' ',C.LastName) AS ReferralBy,
		RS.ReferralBy,
		RS.ReferralStatus,
		RS.CrmAssinged,
		RS.BrokerAssinged,
		RS.IncentivePraposed,
		RS.IsActive
	From
		ReferralStatus As RS
		--,Customer As C
	where
		RS.IsActive !=0
END
GO

