/****** Object:  StoredProcedure [dbo].[Customer_Referral_Delete]    Script Date: 05-07-2018 19:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_Referral_Delete]
(
	@EntityId int,
	@UserType Varchar(20)
	--@IsActive bit
)
AS
BEGIN
   --Delete from [Referral] where ReferralId=@ReferralId  
  
   UPDATE [Referral]
   SET   IsActive=0
   From Referral as R,UserMaster as UM
   WHERE ReferredById=@EntityId
	 And UM.UserType=@UserType

END
GO

