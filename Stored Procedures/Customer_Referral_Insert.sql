/****** Object:  StoredProcedure [dbo].[Customer_Referral_Insert]    Script Date: 05-07-2018 19:15:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* Create Store Procedure for Referral Table to perform Insert Operation*/
/* SP changed:Remove Preferred Loaction,Preferred State,Primary purpose,Source from sp due to Client Requirement*/
/* SP Updated:Change datatypes of BHK And Budget to varchar due to UI Requirement*/
CREATE PROCEDURE [dbo].[Customer_Referral_Insert]         
(   
	--@Id int,
	@EntityId INT ,
	@UserType VARCHAR(50),
    @FirstName VARCHAR(50) ,         
    @LastName VARCHAR(50) , 
	@AddressLine1 VARCHAR(50),
	@City VARCHAR(50),
	@MobileNo VARCHAR(12) ,
	@AlternateNo VARCHAR(12),
	@Email VARCHAR(50),
	@Occupation VARCHAR(30),
	@Relationship VARCHAR(20),
	@RequirementType VARCHAR(50),
	@Budget VARCHAR(15),
	@Bhk VARCHAR(30)=NULL,
	@PreferredCity VARCHAR(50),
	@Source VARCHAR(50)='Referral',
	@VisitSite VARCHAR(10),
	@PreferredDate DATE,
	@PreferredTime time(7),
	@CommercialOption VARCHAR(20)=NULL,
	@ITOption VARCHAR(20)=NULL,
	@NoOfSiteVisit int=0,
	@Face2Face int=0
)      
 
AS
	declare @DateSelected DATE;
	declare @TimeSelected Time(7);   
	declare @BrokerAssinged int;
		   
		if(@PreferredDate='1/1/1753 12:00:00 AM')
				SET @DateSelected=NULL
			Else 
				SET @DateSelected=@PreferredDate

		if (@PreferredTime='1/1/1753 12:00:00 AM')
				SET @TimeSelected=NULL
			Else
				SET @TimeSelected=@PreferredTime

		if(@AddressLine1='')
				SET @AddressLine1='Null'
			Else
				Set @AddressLine1=@AddressLine1

		if(@AlternateNo='')
				SET @AlternateNo='Null'
			Else
				Set @AlternateNo=@AlternateNo

		if(@Relationship='')
				SET @Relationship='Null'
			Else
				Set @Relationship=@Relationship

		if(@Occupation='')
				SET @Occupation='Null'
			Else
				Set @Occupation=@Occupation

		if(@VisitSite='')
				SET @VisitSite='Null'
			Else
				Set @VisitSite=@VisitSite

BEGIN        
	If (@UserType= 'Employee') 
		Set @Source=@Source
		Set @NoOfSiteVisit=@NoOfSiteVisit
		Set	@Face2Face = @Face2Face
	If (@UserType= 'ChannelPartner') 
		Set @BrokerAssinged=@EntityId

	INSERT INTO [Referral] 
	(	
		ReferredById,
		ReferredByType,
		FirstName,
		LastName,
		AddressLine1,
		City,
		MobileNo,
		AlternateNo,
		Email,
		Occupation,
		Relationship,
		RequirementType,
		Budget,
		Bhk,
		PreferredCity,
		[Source],
		VisitSite,
		PreferredDate,
		PreferredTime,	
		IsActive,
		CommercialOption,
		ITOption ,
		CreatedOn,
		NoOfSiteVisit,
		Face2Face
		--,CreatedBy
	)         
    Values
	 (	
		@EntityId  ,
		@UserType ,
		@FirstName ,         
		@LastName  , 
		@AddressLine1,
		@City ,
		@MobileNo  ,
		@AlternateNo ,
		@Email ,
		@Occupation ,
		@Relationship,
		@RequirementType ,
		@Budget ,
		(case when @Bhk is null then 'NA' else @Bhk end),
		@PreferredCity ,
		@Source,
		@VisitSite ,
		@DateSelected,
		@TimeSelected,
		1,
		(case when @CommercialOption is null then 'NA' else @CommercialOption end) ,
	    (case when @ITOption is null then 'NA' else @ITOption end),
		GETDATE(),
		@NoOfSiteVisit,
		@Face2Face
		--,@Id
	)  	
	Declare @Employeeid int;

Set @EmployeeId=(Select Top 10 Percent EmployeeId
	From Employee
	Where 
	Role ='Sales Agent'
	AND City=@PreferredCity
	order By NEWID())

	Select 
	E.Email,
	E.FirstName+' '+E.LastName As Name,
	@FirstName+' '+@LastName As ReferralName,
	( SELECT TOP 
		1 ReferralId 
	from 
		Referral as R 
	Where 
			R.ReferredById=@EntityId
	And	
			R.FirstName=@FirstName
	And
			R.LastName=@LastName
	And
			R.MobileNo=@MobileNo
	And
			R.Email=@Email

	Order By ReferralId DESC
	) As ReferralId
	From Employee As E
	Where 
	E.EmployeeId=@Employeeid 

	Insert Into [ReferralStatus]
	(
		ReferralId,
		ReferralBy,
		ReferralStatus,
		CrmAssinged,
		SalesAgentAssinged,
		BrokerAssinged,
		IncentivePraposed,
		IsActive,
		Source,
		NoOfSiteVisit,
		Face2Face,
		CreatedOn
		--,CreatedBy
	)
	Values
	((
	SELECT TOP 
		1 ReferralId 
	from 
		Referral as R 
	Where 
			R.ReferredById=@EntityId
	And	
			R.FirstName=@FirstName
	And
			R.LastName=@LastName
	And
			R.MobileNo=@MobileNo
	And
			R.Email=@Email

	Order By ReferralId DESC
		),
			(
			Select 
				UM.FirstName+' '+UM.LastName 
			From
				UserMaster As UM 
			Where
				UM.EntityId=@EntityId 
				AND 
				UM.UserType=@UserType
			),
			'New',
			1,
			@EmployeeId,
			@BrokerAssinged,
			'NA',
			1,
			@Source,
			@NoOfSiteVisit,
			@Face2Face,
			GETDATE()
			--,@Id
	)

End
GO

