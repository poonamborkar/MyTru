/****** Object:  StoredProcedure [dbo].[Customer_Referral_PreferredBHK_Select]    Script Date: 05-07-2018 19:15:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_Referral_PreferredBHK_Select]
(
@CustomerId int
)
AS
BEGIN

Select

PD.BHK
from
Referral as RF,
ProjectDetails as PD,
Customer as C,
ProjectMaster as PM

Where

PM.ProjectId=PD.ProjectId
AND
RF.ReferredById=C.CustomerId
AND
RF.PreferredLocation= PM.Location
AND
C.CustomerId=1

END
GO

