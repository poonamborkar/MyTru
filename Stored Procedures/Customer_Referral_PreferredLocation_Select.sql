/****** Object:  StoredProcedure [dbo].[Customer_Referral_PreferredLocation_Select]    Script Date: 05-07-2018 19:16:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Customer_Referral_PreferredLocation_Select]

(
@CustomerId int
)

AS
BEGIN

Select
RF.PreferredLocation

from

Referral as RF,
ProjectMaster as PM,
Customer as C

WHERE

RF.PreferredLocation= PM.Location
AND
RF.ReferralId= C.CustomerId
AND
RF.PreferredCity= PM.City
AND
C.CustomerId=@CustomerId
END
GO

