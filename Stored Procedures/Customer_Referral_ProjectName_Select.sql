/****** Object:  StoredProcedure [dbo].[Customer_Referral_ProjectName_Select]    Script Date: 05-07-2018 19:16:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_Referral_ProjectName_Select]
(
@CustomerId int
)
AS
BEGIN

Select
ProjectName

FROM

ProjectMaster AS PM,
Customer AS C,
Referral AS RF

WHERE

PM.Location= RF.PreferredLocation
AND
RF.ReferralId= C.CustomerId
AND
C.CustomerId=@CustomerId


END
GO

