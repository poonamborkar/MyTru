/****** Object:  StoredProcedure [dbo].[Customer_Referral_Select]    Script Date: 05-07-2018 19:16:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Create Stored Procedure Template for Azure SQL  Database
-- =======================================================
CREATE PROCEDURE [dbo].[Customer_Referral_Select]
(
	@EntityId int,
	@UserType varchar(20)

)
AS
BEGIN
  SELECT Distinct

		ReferralId,
		Title,
		R.FirstName,
		R.LastName,
		AddressLine1,
		AddressLine2,
		Landmark,
		Area,
		City,
		[State],
		Country,
		Pincode,
		R.MobileNo,
		AlternateNo,
		Email,
		Occupation,
		Relationship,
		RequirementType,
		Budget,
		Bhk,
		PreferredCity,
		PreferredLocation,
		PrimaryPurpose,
		[Source],
		VisitSite,
		CONVERT(NVARCHAR,PreferredDate,106) AS PreferredDates,
		PreferredTime,
		ReferredById,
		ReferredByType,
		R.IsActive,
		PreferredState,
		R.CreatedBy,
		R.CreatedOn,
		R.UpdatedBy,
		R.UpdatedOn,
		R.CommercialOption,
		R.ITOption

	FROM
		Referral as R, UserMaster as UM

	WHERE
		
		ReferredById=@EntityId AND UM.UserType=@UserType 
		And 
	    R.IsActive !=0
END
GO

