/****** Object:  StoredProcedure [dbo].[Customer_Referral_SelectAll]    Script Date: 05-07-2018 19:17:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_Referral_SelectAll]
AS
BEGIN
  SELECT 
		
		ReferralId,
		Title,
		FirstName,
		LastName,
		AddressLine1,
		AddressLine2,
		Landmark,
		Area,
		City,
		[State],
		Country,
		Pincode,
		MobileNo,
		AlternateNo,
		Email,
		Occupation,
		Relationship,
		RequirementType,
		Budget,
		Bhk,
		PreferredCity,
		PreferredLocation,
		PrimaryPurpose,
		[Source],
		VisitSite,
		CONVERT(NVARCHAR,PreferredDate,106) AS PreferredDate,
		PreferredTime,
		ReferredById,
		ReferredByType,
		IsActive,
		PreferredState,
		CreatedOn,
		CreatedBy,
		UpdatedOn,
		UpdatedBy,
		CommercialOption,
		ITOption
	FROM
		[Referral]
	WHERE
		IsActive !=0

END
GO

