/****** Object:  StoredProcedure [dbo].[Customer_Referral_Update]    Script Date: 05-07-2018 19:17:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Customer_Referral_Update]
(
	--@Id int,
    @ReferralId int,
	@Title VARCHAR(6),
    @FirstName VARCHAR(50) ,         
    @LastName VARCHAR(50) , 		       
    @AddressLine1 VARCHAR(50),
	--@AddressLine2 VARCHAR(50),
	--@Landmark VARCHAR(50),
	--@Area VARCHAR(50),
	@City VARCHAR(50),
	--@State VARCHAR(50),
	--@Country VARCHAR(50),
	--@Pincode INT,	        
    @MobileNo VARCHAR(12) ,
	@AlternateNo VARCHAR(12),
	@Email VARCHAR(50),
	@Occupation VARCHAR(30),
	@Relationship VARCHAR(20),
	@RequirementType VARCHAR(50),
	@Budget VARCHAR(15),
	@Bhk VARCHAR(20),
	@PreferredCity VARCHAR(50),
	--@PreferredLocation VARCHAR(50),
	--@PrimaryPurpose VARCHAR(50),
	--@Source VARCHAR(50),
	@VisitSite VARCHAR(10),
	@PreferredDate DATE,
	@PreferredTime time(7),
	@ReferredById INT ,
	@ReferredByType VARCHAR(50),
	@CommercialOption VARCHAR(20),
	@ITOption VARCHAR(20),
	@Face2Face int,
	@NoOfSiteVisit int
	--@IsActive BIT,
	--@PreferredState VARCHAR(50)
	--@UpdatedOn DATE
)
AS
BEGIN
	UPDATE [Referral]
	SET

		 Title=@Title,
		 FirstName=@FirstName,
		 LastName=@LastName,

		 AddressLine1= @AddressLine1,
		-- AddressLine2= @AddressLine2,
		-- Landmark= @Landmark,
		-- Area=@Area ,
		 City=@City,
		 --[State]=@State,
		 --Country=@Country,
		 --Pincode=@Pincode,

		 MobileNo= @MobileNo ,
		 AlternateNo=@AlternateNo,
		 Email=@Email ,

		 Occupation= @Occupation,
		 Relationship=@Relationship,

		 RequirementType=@RequirementType,
		 Budget=@Budget,
		 Bhk=@Bhk,
		 PreferredCity=@PreferredCity,
		 --PreferredLocation=@PreferredLocation,
		 --PrimaryPurpose=@PrimaryPurpose,
		 --[Source]=@Source,
		 VisitSite=@VisitSite,
		 PreferredDate=@PreferredDate,
		 PreferredTime=@PreferredTime,
		 ReferredById=@ReferredById,
		 ReferredByType=@ReferredByType,
		 CommercialOption=@CommercialOption,
		 ITOption=@ITOption,
		 Face2Face=@Face2Face,
		 NoOfSiteVisit=@NoOfSiteVisit
		-- IsActive=@IsActive,
		 --PreferredState=@PreferredState
		 --,UpdatedBy=@Id,
		 --UpdatedOn=@UpdatedOn
		 WHERE ReferralId=@ReferralId
		 AND IsActive=1
   
END
GO

