/****** Object:  StoredProcedure [dbo].[Customer_ServiceRequest_Delete]    Script Date: 05-07-2018 19:18:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ServiceRequest_Delete]
(
 @IssueId int
)
AS
BEGIN
   UPDATE[Customer_ServiceRequest] 
      
   SET IsActive=0 
   --from Customer_ServiceRequest

   where IssueId=@IssueId
END
GO

