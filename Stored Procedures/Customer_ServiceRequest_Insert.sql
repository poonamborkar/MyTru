/****** Object:  StoredProcedure [dbo].[Customer_ServiceRequest_Insert]    Script Date: 05-07-2018 19:19:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ServiceRequest_Insert]
		(
		@EntityId INT,
		@ProjectName VARCHAR(50),
		@Building VARCHAR(20),
		@Flat VARCHAR(20),
		@IssueSubject VARCHAR(50),
		@IssueType VARCHAR(100),
		@Details VARCHAR(500),
		@IssueDate DATE,
		@UserType varchar(15)
	--	@Id int
		)
AS
BEGIN
	INSERT INTO  [Customer_ServiceRequest]
		(
		EnteredById,
		ProjectId,
		EmployeeId,
		EnteredByType,
		ProjectName,
		Building,
		Flat,
		IssueStatus,
		IssueSubject,
		IssueType,
		Details,
		IssueDate,
		IsActive,
		CreatedOn,
		CreatedBy
		)
	VALUES
		(
		@EntityId,
		(Select TOP 1 ProjectId from ProjectMaster where ProjectName = @ProjectName),
		19,
		@UserType,
		@ProjectName,
		@Building,
		@Flat,
		'new',
		@IssueSubject,
		@IssueType,
		@Details,
		@IssueDate,
		1,
		GETDATE(),
		'UserId'
		)

		--This new Code is Added as per requirment to get result of Above Insert Query

Declare @RequestId int;				
Set @RequestId=				--Get Top 1st IssueId From ServiceRequest Table
	(				
		SELECT TOP 
			1 IssueId 
		from 
			Customer_ServiceRequest as R,
			UserMaster As UM
		Where 
			UM.EntityID=@EntityId
		And	
			UM.UserType=@UserType
		AND		
			R.ProjectName=@ProjectName
		AND	
			R.IssueSubject = @IssueSubject
		Order By 
			R.IssueId 
		DESC
	)

Select 
		@RequestId As IssueId,
		R.EnteredById,
		R.ProjectId,
		R.EmployeeId,
		R.EnteredByType,
		R.ProjectName,
		R.Building,
		R.Flat,
		R.IssueStatus,
		R.IssueSubject,
		R.IssueType,
		R.Details,
		R.IssueDate,
		R.IsActive,
		R.CreatedOn,
		R.CreatedBy,
		UM.UserId,
		UM.MobileNo,
		UM.FirstName + ' ' + UM.LastName as [Name]
		From 
		Customer_ServiceRequest As R,
		UserMaster As UM
		Where 
		R.EnteredById=UM.EntityId AND
		R.EnteredByType=UM.UserType AND
		R.IssueId=@RequestId 

END
GO

