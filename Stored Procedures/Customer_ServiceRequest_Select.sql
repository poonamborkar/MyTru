/****** Object:  StoredProcedure [dbo].[Customer_ServiceRequest_Select]    Script Date: 05-07-2018 19:19:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ServiceRequest_Select]
(
	@EntityId INT,
	@UserType Varchar(20)
)
AS
BEGIN
	SELECT
		IssueId,
		EnteredById as EntityId,
		ProjectName,
		Building,
		Building + '-' + Flat as Flat,
		IssueStatus,
		IssueSubject,
		IssueType,
		Details,
		CONVERT(NVARCHAR,IssueDate,120) AS [IssueDate],
		IsActive

FROM [dbo].[Customer_ServiceRequest] 
WHERE EnteredById=@EntityId
AND EnteredByType=@UserType
AND IsActive=1--To Find Table is Deleted or not
Order By IssueId Desc 
END
GO

