/****** Object:  StoredProcedure [dbo].[Customer_ServiceRequest_SelectAll]    Script Date: 05-07-2018 19:20:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ServiceRequest_SelectAll]
AS
BEGIN
	SELECT --------------------------------Select ALL RECORD FROM Customer_ServiceRequest Table-----------
		IssueId,
		EnteredById,
		EnteredByType,
		ProjectName,
		Building,
		Flat,
		IssueSubject,
		IssueType,
		Details,
		CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
		IsActive

	FROM Customer_ServiceRequest
	Where IsActive=1      --To Find Table is Selected or not
END
GO

