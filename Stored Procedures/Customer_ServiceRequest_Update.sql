/****** Object:  StoredProcedure [dbo].[Customer_ServiceRequest_Update]    Script Date: 05-07-2018 19:20:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ServiceRequest_Update]
(
	@IssueId int,
	@EntityId INT,
	@UserType varchar(15),
	@ProjectName VARCHAR(50),
	@Building VARCHAR(20),
	@Flat VARCHAR(20),
	@IssueSubject VARCHAR(50),
	@IssueType VARCHAR(100),
	@Details VARCHAR(500),
	@IssueDate Date
	--@Id int
)
AS
BEGIN
	declare @UpdateOnDate varchar(20);
	Set @UpdateOnDate=  convert(varchar, getdate(), 1);


	UPDATE Customer_ServiceRequest
	SET
		EnteredById=@EntityId,
		ProjectName=@ProjectName,
		Building=@Building,
		Flat=@Flat,
		IssueSubject=@IssueSubject,
		IssueType=@IssueType,
		Details = Customer_ServiceRequest.Details+','+ CHAR(13) +@Details+'(EnteredBy:'+(select FirstName+' '+LastName from UserMaster where UserType=@UserType and EntityId=@EntityId)+',EnteredOn:'+@UpdateOnDate+')',
		IssueDate=@IssueDate,
		--UpdatedBy = @Id,
		UpdatedOn= GETDATE()
	WHERE EnteredById=@EntityId
		And EnteredByType=@UserType
		And IssueId=@IssueId
		AND IsActive=1
END
GO

