/****** Object:  StoredProcedure [dbo].[Customer_Social_Delete]    Script Date: 05-07-2018 19:20:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_Social_Delete]
(
	@SocialId int
)
AS
BEGIN

UPDATE[Customer_Social] 
      
   SET IsActive='False' From Customer_Social

   WHERE SocialId=@SocialId
END
GO

