/****** Object:  StoredProcedure [dbo].[Customer_ViewProfile_Delete]    Script Date: 05-07-2018 19:22:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_ViewProfile_Delete]
(
	@CustomerId int
)
AS
BEGIN
DELETE FROM 
	Customer
WHERE
	CustomerId=@CustomerId    
END
GO

