/****** Object:  StoredProcedure [dbo].[Customer_ViewProfile_Select]    Script Date: 05-07-2018 19:22:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_ViewProfile_Select]  --'1'    
@EntityId int,
@UserType varchar(50)
as      
If (@UserType = 'customer')
Begin  
	select 
		C.CustomerId,		
		(C.FirstName+' '+C.LastName) AS Name,
		C.AddressLine1,  
		C.Landmark, 
		C.Area, 
		C.City, 
		C.[State], 
		C.Country, 
		C.Pincode, 
		C.MobileNo, 
		C.AlternateNo, 
		C.Email, 
		C.Occupation, 
		--C.Dob,
		CONVERT(NVARCHAR,Dob,120) AS Dob,
		C.CrmAgent from Customer (nolock) as C
where C.CustomerId =@EntityId
select 
		CO.OrderId, 
		CO.ProjectId, 
		CO.FlatNo, 
		CO.BuildingType,
		CO.BHK,
		PM.ProjectName, 
		PM.Longitude, 
		PM.Latitude, 
		CO.EmployeeId, 
		CO.SalesAgentId 
		from 
		CustomerOrder as CO,
		ProjectMaster as PM,
		Customer as C
		where
		
		C.CustomerId = @EntityId
		AND
		C.CustomerId = CO.CustomerId
		AND
		PM.ProjectId = CO.ProjectId
End

Else

Begin
select  
		RF.ReferralId As CustomerId,
		(RF.Title+RF.FirstName+' '+RF.LastName) AS Name,
		RF.AddressLine1, 
		RF.Landmark, 
		RF.Area, 
		RF.City, 
		RF.[State], 
		RF.Country, 
		RF.Pincode, 
		RF.MobileNo, 
		RF.AlternateNo, 
		RF.Email, 
		RF.Occupation, 
		--RF.Dob
		CONVERT(NVARCHAR,Dob,120) AS Dob
		from Referral (nolock) as RF
where RF.ReferralId = @EntityId

End





GO

