/****** Object:  StoredProcedure [dbo].[Customer_ViewProfile_Update]    Script Date: 05-07-2018 19:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Customer_ViewProfile_Update]
		@EntityId int,
		@UserType varchar(50),	
		@AddressLine1 varchar(50),
		@City Varchar(50),
		@State Varchar(50),
		@Country Varchar(50),
		@Pincode int,
		@AlternateNo Varchar(12),
		@Occupation Varchar(50)
	--	@Id int
as   

if(@UserType = 'customer')
      
Begin 
	Update Customer
	set  
		AddressLine1=@AddressLine1, 
		City=@City, 
		State=@State, 
		Country=@Country,
		Pincode=@Pincode, 
		AlternateNo=@AlternateNo, 
		Occupation=@Occupation
		--,
		--UpdatedBy=@Id
		
WHERE 
		CustomerId=@EntityId 	       
End
Else
Begin 
	Update Referral
	set  
		AddressLine1=@AddressLine1,

		City=@City, 
		State=@State, 
		Country=@Country,
		Pincode=@Pincode,  
		AlternateNo=@AlternateNo, 
		Occupation=@Occupation
		--, 
		--UpdatedBy=@Id
		
Where 
		ReferralId=@EntityId 		       
End

GO

