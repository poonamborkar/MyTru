/****** Object:  StoredProcedure [dbo].[EmailNotification_On_Order_Submit]    Script Date: 05-07-2018 19:26:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EmailNotification_On_Order_Submit]
AS
BEGIN

	Select 
		R.ReferralId as ReferralId,
		R.FirstName+' '+R.LastName as ReferralName,
		R.Email AS [cc],
		UM.UserId as UserId,
		UM.FirstName+' '+UM.LastName as ReferredByName	
	from 
		Referral as R inner join
		CustomerOrder as CO
	ON
		R.ReferralId=CO.CustomerId inner join
		UserMaster As UM
	ON
		R.ReferredById=UM.EntityId and R.ReferredByType=UM.UserType
	Where
		R.ReferralId = CO.CustomerId
	AND 
		R.ReferredById=UM.EntityId
	AND 
		CO.AgreementStatus='Submit'
	AND
		UM.UserType=R.ReferredByType

END


GO

