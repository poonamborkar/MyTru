/****** Object:  StoredProcedure [dbo].[EmailNotification_Referral_OneDayReminder]    Script Date: 05-07-2018 19:27:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EmailNotification_Referral_OneDayReminder]
AS 
BEGIN
	Select 
	  RS.ReferralId,
	  R.FirstName+' '+R.LastName AS ReferName,
	  E.FirstName+' '+E.LastName AS [Name],
	  RS.ReferralStatus,
	  UM.UserId As UserId,
	  (Select UserId from UserMaster Where EntityId = E.ReportingTo And UserType = 'Employee') AS [cc]
	 from 
	  Referral as R inner join 
	  ReferralStatus as RS
	 On 
	  R.ReferralId = RS.ReferralId inner join
	  Employee As E
	 On 
	  RS.SalesAgentAssinged=E.EmployeeId inner join
	  UserMaster As UM
	 On
	  RS.SalesAgentAssinged = UM.EntityId
	 Where  
	  RS.ReferralStatus='New' and RS.CreatedOn <= DATEADD(HOUR, -24, GETDATE())
	 And 
	  UM.UserType='Employee'
END	 
	
		


GO

