/****** Object:  StoredProcedure [dbo].[EmployeeDashboard_ServiceRequest_Update]    Script Date: 05-07-2018 19:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EmployeeDashboard_ServiceRequest_Update] 
(	
	@IssueStatus Varchar(50),
	@AddDetails varchar(100),
	@IssueId int,
	@UserType varchar(20),
	@EntityId int
	--@Id int
) 
AS
	declare @UpdateOnDate varchar(20);
	Set @UpdateOnDate=  convert(varchar, getdate(), 1);

BEGIN  
	   UPDATE Customer_ServiceRequest
		SET IssueStatus=@IssueStatus,
			Details = Customer_ServiceRequest.Details+','+@AddDetails+'(EnteredBy:'+(select FirstName+' '+LastName from UserMaster where UserType=@UserType and EntityId=@EntityId)+',EnteredOn:'+@UpdateOnDate+')'
		--	UpdatedBy = @Id	
		WHERE IssueId=@IssueId;



	  
END
GO

