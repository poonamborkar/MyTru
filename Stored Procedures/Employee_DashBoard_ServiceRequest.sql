/****** Object:  StoredProcedure [dbo].[Employee_DashBoard_ServiceRequest]    Script Date: 05-07-2018 19:28:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_DashBoard_ServiceRequest] 
(	
	@UserType Varchar(30),
	@EntityId int
) 
AS
	declare @EnteredByType varchar(20);
			Set @EnteredByType=@UserType
	declare @EnteredById int;
			Set @EnteredById=@EntityId
	
Begin
	If(@EnteredByType='Customer')
	Begin
        Select distinct 
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive
		from Customer_ServiceRequest As RS 
		Where RS.EnteredById=@EntityId And RS.EnteredByType='Customer'
	End
   Else If(@EnteredByType='Employee')
	Begin
        Select distinct 
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive
		from Customer_ServiceRequest As RS 
		Where RS.EnteredById=@EntityId And RS.EnteredByType='Employee'
	End
	Else If(@EnteredByType='ChannelPartner')
	Begin
		Select distinct 
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive
		from Customer_ServiceRequest As RS 
		Where RS.EnteredById=@EntityId And RS.EnteredByType='ChannelPartner'
	End
	Else
	Begin
		Select distinct 
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive
		from Customer_ServiceRequest As RS 
		Where RS.EnteredById=@EntityId
	End
End
GO

