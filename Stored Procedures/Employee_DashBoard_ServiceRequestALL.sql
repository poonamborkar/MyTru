/****** Object:  StoredProcedure [dbo].[Employee_DashBoard_ServiceRequestALL]    Script Date: 05-07-2018 19:29:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_DashBoard_ServiceRequestALL] 
(	
	@UserType Varchar(30),
	@EntityId int
) 
AS
Begin
        Select  
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive
		from Customer_ServiceRequest As RS 
		Where RS.EnteredById=@EntityId					--Get Service Request By EnteredById
		AND RS.EnteredByType=@UserType
End
GO

