/****** Object:  StoredProcedure [dbo].[Employee_DashBoard_ServiceRequest_Employee]    Script Date: 05-07-2018 19:28:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[Employee_DashBoard_ServiceRequest_Employee] 
(	
	@UserType Varchar(30),
	@EntityId int
) 
AS
Begin
        Select  
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive
		from Customer_ServiceRequest As RS 
		Where RS.EmployeeId=@EntityId 
End
GO

