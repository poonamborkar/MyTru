/****** Object:  StoredProcedure [dbo].[Employee_DashBoard_ViewDetail]    Script Date: 05-07-2018 19:29:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_DashBoard_ViewDetail] 
(	
	@UserType Varchar(30) ,
	@EntityId int
) 
AS
	Declare @result Varchar(20);	
Begin
	if(@UserType='Customer')	
	Begin
		Select  C.CustomerId,
				C.Title, 
				C.FirstName, 
				C.LastName, 
				C.AddressLine1, 
				C.AddressLine2, 
				C.Landmark, 
				C.Area, 
				C.City, 
				C.[State], 
				C.Country, 
				C.Pincode, 
				C.MobileNo, 
				C.AlternateNo, 
				C.Email, 
				C.Occupation, 
				CONVERT(NVARCHAR,Dob,120) AS Dob,
				C.CrmAgent,
				C.IsActive
		from Customer as C 
		Where C.CustomerId=@EntityId
	End
   Else If(@UserType='Employee')
	Begin
		Select E.EmployeeId,
			   E.Title,
			   E.FirstName,
			   E.LastName,
			   E.MobileNo,
			   E.AlternateNo,
			   E.[Role],
			   E.AssingedArea,
			   E.AssingedProject1,
			   E.AssingedProject2,
			   E.AssingedProject3,
			   E.Manager,
			   E.StreetAddress1,
			   E.StreetAddress2,
			   E.Landmark,
			   E.Area,
			   E.City,
			   E.[State],
			   E.Country,
			   E.PinCode,
			   E.Email,
			   E.Occupation,
			   E.EmergancyContact
		from Employee as E 
		Where E.EmployeeId=@EntityId
	End
	Else If(@UserType='Broker')
	Begin
		Select B.BrokerId,
			   B.BrokerUserId,
			   B.Title,
			   B.FirstName,
			   B.LastName,
			   B.AddressLine1,
			   B.AddressLine2,
			   B.Landmark,
			   B.Area,
			   B.City,
			   B.[State],
			   B.Country,
			   B.Pincode,
			   B.AlternateNumber,
			   B.DateOfBirth,
			   B.CrmAgent,
			   B.IsActive,
			   B.MobileNo,
			   B.Email,
			   B.Occupation
		from BrokerMaster as B 
		Where B.BrokerId=@EntityId
	End
	Else
	Begin
		Select R.ReferralId,
			   R.Title,
			   R.FirstName,
			   R.LastName,
			   R.AddressLine1,
			   R.AddressLine2,
			   R.Landmark,
			   R.Area,
			   R.City,
			   R.[State],
			   R.Country,
			   R.Pincode,
			   R.MobileNo,
			   R.Email,
			   R.Occupation,
			   R.Relationship,
		   	   R.RequirementType,
			   R.Budget,
			   R.Bhk,
			   R.PreferredCity,
			   R.PreferredLocation,
			   R.PrimaryPurpose,
			   R.[Source],
			   R.VisitSite,
			   CONVERT(NVARCHAR,PreferredDate,106) AS PreferredDates,
			   R.PreferredTime,
			   R.ReferredById,
			   R.ReferredByType,
		       R.IsActive,
			   R.PreferredState,
			   R.CommercialOption,
			   R.ITOption		 		 
		from Referral as R 
		Where R.ReferralId=@EntityId
	End
End
GO

