/****** Object:  StoredProcedure [dbo].[Employee_Dashboard_IssueStatus]    Script Date: 05-07-2018 19:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_Dashboard_IssueStatus]
AS
BEGIN

Select count(IssueId) AS NoOfIssues,IssueStatus 
FROM Customer_ServiceRequest 
GROUP BY IssueStatus

END
GO

