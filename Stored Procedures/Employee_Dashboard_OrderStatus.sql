/****** Object:  StoredProcedure [dbo].[Employee_Dashboard_OrderStatus]    Script Date: 05-07-2018 19:27:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_Dashboard_OrderStatus]
AS
BEGIN

Select count(OrderId) AS NoOfOrders,OrderStatus 
FROM CustomerOrder 
GROUP BY OrderStatus

END
GO

