/****** Object:  StoredProcedure [dbo].[Employee_Dashboard_ReferralStatus]    Script Date: 05-07-2018 19:28:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_Dashboard_ReferralStatus]
AS
BEGIN

Select count(ReferralId) AS NoOfReferrals,ReferralStatus 
FROM ReferralStatus 
GROUP BY ReferralStatus

END
GO

