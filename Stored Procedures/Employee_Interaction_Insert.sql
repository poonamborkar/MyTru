/****** Object:  StoredProcedure [dbo].[Employee_Interaction_Insert]    Script Date: 05-07-2018 19:29:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Employee_Interaction_Insert]
	(
		@EntityId int ,
		@UserType varchar(50),
		@InteractionType Varchar(50),
		@Details varchar(400),
		@IssueId Int,
		@EmployeeId INT,
		@FollowUpRequired Varchar(10)
		--@Id int
	)
As
Begin
INSERT into Interaction
	(  
		EntityId ,
		UserType,
		InteractionType,
		Details,
		IssueId,
		EmployeeId,
		FollowUpRequired,
		--CreatedBy,
		CreatedOn,
		IsActive 
	)
	Values
	(
		@EntityId ,
		@UserType,
		@InteractionType,
		@Details,
		@IssueId,
		@EmployeeId,
		@FollowUpRequired,
		--@Id,
		GetDate(),
		'TRUE' 
	)
End
GO

