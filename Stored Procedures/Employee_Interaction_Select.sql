/****** Object:  StoredProcedure [dbo].[Employee_Interaction_Select]    Script Date: 05-07-2018 19:30:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Employee_Interaction_Select]
	(
@EntityId int,
@UserType Varchar(20)
	)
AS 
Begin

	If (@UserType='Referral')
	Begin

	Select 
		I.InteractionId,
		I.EntityId,
		I.UserType,
		I.InteractionType,
		I.Details,
		I.IssueId,
		I.EmployeeId,
		I.FollowUpRequired
From 
		Interaction AS I,
		Referral As R
Where 
		I.EntityId=@EntityId AND
		I.EntityId = R.ReferralId
		And I.IsActive = 'True' 
	END
Else
BEGIN
Select 
		I.InteractionId,
		I.EntityId,
		I.UserType,
		I.InteractionType,
		I.Details,
		I.IssueId,
		I.EmployeeId,
		I.FollowUpRequired
From 
		Interaction AS I,
		UserMaster As UM
Where 
		I.EntityId=@EntityId AND
		I.EntityId = UM.EntityId
		And I.IsActive = 'True' AND
		UM.UserType=@UserType
End
END
GO

