/****** Object:  StoredProcedure [dbo].[Employee_Interaction_SelectAll]    Script Date: 05-07-2018 19:30:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Employee_Interaction_SelectAll]
	(
@EntityId int,
@UserType Varchar(20)
	)
AS 
Begin
Select 
		I.InteractionId,
		I.EntityId,
		I.UserType,
		I.InteractionType,
		I.Details,
		I.IssueId,
		I.EmployeeId,
		I.FollowUpRequired
From 
		Interaction AS I
Where 
		I.EmployeeId=@EntityId 
		And I.IsActive = 'True'
	
End
	
GO

