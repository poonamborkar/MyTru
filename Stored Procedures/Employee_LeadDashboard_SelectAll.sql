/****** Object:  StoredProcedure [dbo].[Employee_LeadDashboard_SelectAll]    Script Date: 05-07-2018 19:31:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_LeadDashboard_SelectAll] --26,'Employee'
(
@EntityId int,
@UserType Varchar(25)
)
AS
BEGIN
		SELECT 
		RS.ReferralId,
		RS.ReferralBy,
		RS.CrmAssinged,
		RS.BrokerAssinged,
		RS.IncentivePraposed,
		(R.FirstName +' '+ R.LastName) AS Name,
		RS.ReferralStatus,
		R.CreatedOn,
		R.PreferredCity,
		RS.Source,
		RS.Face2Face,
		RS.NoOfSiteVisit,
		RS.SalesAgentAssinged
	FROM
		ReferralStatus As RS (nolock)
	Inner Join
		Referral As R (nolock)
	On 
		RS.ReferralId= R.ReferralId 
	Inner Join
		Employee AS E (nolock)
	On 
		RS.SalesAgentAssinged = E.EmployeeId
	WHERE
		RS.ReferralStatus <> 'Closed' AND 
		RS.IsActive !=0 
		AND DATEDIFF(day,R.CreatedOn,getdate()) between 0 and 90 
		AND
		E.EmployeeId in (Select EmployeeId from Employee where ReportingTo = @EntityId)
	OR 
		E.EmployeeId in (Select EmployeeId from Employee where ReportingTo in (Select EmployeeId from Employee where ReportingTo = @EntityId))
	OR
		E.EmployeeId=@EntityId

END


GO

