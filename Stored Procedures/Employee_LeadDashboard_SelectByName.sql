/****** Object:  StoredProcedure [dbo].[Employee_LeadDashboard_SelectByName]    Script Date: 05-07-2018 19:31:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_LeadDashboard_SelectByName] 
(
	@FromDate Date =NULL,
	@ToDate Date=NULL,
	@Id Varchar(35)=NULL,
	@EntityId int,
	@UserType Varchar(30)
)
AS
BEGIN
	if (@Id=0)														--If Select All Is Selected Then 
		Exec Employee_LeadDashboard_SelectAll @EntityId,@UserType   --Execute Select_All And Pass Sesion Entityid
	Else															--Else Show Result of Employee
  SELECT 
		RS.ReferralId,
		RS.ReferralBy,
		RS.CrmAssinged,
		RS.BrokerAssinged,
		RS.IncentivePraposed,
		(R.FirstName +' '+ R.LastName) AS Name,
		ReferralStatus,
		CONVERT(date, R.CreatedOn) As CreatedOn,
		R.PreferredCity,
		RS.Source,
		RS.Face2Face,
		RS.NoOfSiteVisit,
		RS.SalesAgentAssinged
	FROM
		ReferralStatus As RS (nolock)
	Inner Join
		Referral As R (nolock)
	On 
		RS.ReferralId= R.ReferralId 
	inner join
		Employee AS E (nolock)
	On
		(RS.SalesAgentAssinged=E.EmployeeId OR RS.CrmAssinged=E.EmployeeId )
	WHERE
		RS.IsActive !=0 
		AND
		E.EmployeeId =@Id
END

GO

