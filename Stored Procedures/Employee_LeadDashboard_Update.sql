/****** Object:  StoredProcedure [dbo].[Employee_LeadDashboard_Update]    Script Date: 05-07-2018 19:31:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_LeadDashboard_Update] 
(
@ReferralId int,
@ReferralStatus Varchar(20),
@Face2Face int= null,
@NoOfSiteVisit int= null,
@ProjectVisited varchar(30)=null,
@EntityId int,
@Remarks varchar(200)=NULL
--@Id int
)
AS
BEGIN

If(@Remarks IS  NOT NULL )
BEGIN
		INSERT into Interaction
	(  
		EntityId ,
		UserType,
		InteractionType,
		Details,
		EmployeeId,
		CreatedOn,
		--CreatedBy,
		IsActive 
	)
	Values
	(
		@ReferralId ,
		'Referral',
		@ReferralStatus,
		@Remarks,
		@EntityId,
		GetDate(),
		--@Id,
		'TRUE' 
	)

END


BEGIN
		Update ReferralStatus
		SET
		ReferralStatus=@ReferralStatus,
		Face2Face=@Face2Face,
		NoOfSiteVisit=@NoOfSiteVisit,
		ProjectVisited=@ProjectVisited
		--UpdatedBy=@Id
	WHERE
		IsActive !=0 And
		ReferralId=@ReferralId
		END
END


GO

