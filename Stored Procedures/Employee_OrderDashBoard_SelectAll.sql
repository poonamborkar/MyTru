/****** Object:  StoredProcedure [dbo].[Employee_OrderDashBoard_SelectAll]    Script Date: 05-07-2018 19:32:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_OrderDashBoard_SelectAll] 
(
@CustomerId int = NULL,
@ProjectName varchar(30) = NULL,
@BuildingType varchar(30) = NULL,
@BookingDateFrom Date = NULL,
@BookingDateTo Date = NULL,
@EntityId int,
@UserType Varchar(25)
)
AS
BEGIN
		SELECT Distinct
	CO.OrderId,
	CO.CustomerId,
	(C.FirstName + ' ' + C.LastName) AS CustomerName,
	C.MobileNo,
	C.Email,
	CO.JointName1,
	CO.JointName2,
	CO.ProjectId,
	PM.ProjectName,
	CO.BuildingType,
	CO.ProjectType,
	CO.LayoutType,
	CO.BHK,
	CO.FlatNumber,
	CO.SalesAgentId,
	CO.EmployeeId,
	CO.BrokerId,
	CO.AgreementStatus,
	CO.AgreedCost,
	CO.TotalValue,
	CO.BookingAmount,
	CO.Remarks,
	CO.CreatedBy,
	CO.UpdatedBy,
	CO.BookingDate,
	CO.AgreementDate,
	CO.BookingDay,
	CO.Scheme,
	CO.Rate,
	CO.TotalDue,
	CO.RegistrationNo,
	CO.Source,
	CO.SourceDetails,
	CO.Bank,
	CO.PANNo1,
	CO.PANNo2,
	CO.PANNo3,
	CO.StampDuty,
	CO.RegistrationTax,
	CO.TDS,
	CO.GST,
	CO.Parking,
	CO.FloorRise,
	CO.PLC,
	CO.InfraCharge,
	CO.CorrespondenceAddress,
	PD.CarpetArea,
	PD.SaleableArea,
	PD.AttachedTerrace,
	PD.EnclosedBalcony,
	PD.ParkingSlot,
	CO.MaintenanceDeposit
	FROM
		CustomerOrder As CO (nolock)
	Inner Join
		Customer As C (nolock)
	On 
		CO.CustomerId= C.CustomerId 
	Inner Join
		Employee AS E (nolock)
	On 
		(CO.SalesAgentId = E.EmployeeId OR CO.EmployeeId=E.EmployeeId)
	Inner Join
		ProjectMaster As PM 
	On
		CO.ProjectId=PM.ProjectId
	Inner Join
		ProjectDetails As PD
	On	
		PM.ProjectId=PD.ProjectId AND CO.FlatNumber=PD.FlatNo AND CO.BuildingType=CO.BuildingType AND CO.BHK=PD.BHK
	WHERE
		CO.IsActive !=0 
		--AND DATEDIFF(day,R.CreatedOn,getdate()) between 0 and 90 
		AND
		E.EmployeeId in (Select EmployeeId from Employee where ReportingTo = @EntityId)
	OR 
		E.EmployeeId in (Select EmployeeId from Employee where ReportingTo in (Select EmployeeId from Employee where ReportingTo = @EntityId))
	OR
		E.EmployeeId=@EntityId

END


GO

