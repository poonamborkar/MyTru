/****** Object:  StoredProcedure [dbo].[Employee_Order_SelectByName]    Script Date: 05-07-2018 19:32:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_Order_SelectByName] 
(
	@BookingDateFrom Date =NULL,
	@BookingDateTo Date=NULL,
	@Name Varchar(35)=NULL,
	@EntityId int,
	@UserType Varchar(30)
)
AS
BEGIN
	if (@Name='ALL')
		Exec CustomerOrder_Order_Select @EntityId,@UserType
	Else
  SELECT 
		CO.OrderId,
		CO.CustomerId,
		(C.FirstName + ' ' + C.LastName) AS CustomerName,
		C.MobileNo,
		C.Email,
		CO.JointName1,
		CO.JointName2,
		CO.ProjectId,
		PM.ProjectName,
		CO.BuildingType,
		CO.ProjectType,
		CO.LayoutType,
		CO.BHK,
		CO.FlatNumber,
		CO.SalesAgentId,
		CO.EmployeeId,
		CO.BrokerId,
		CO.AgreementStatus,
		CO.AgreedCost,
		CO.TotalValue,
		CO.BookingAmount,
		CO.Remarks,
		CO.CreatedBy,
		CO.UpdatedBy,
		CO.BookingDate,
		CO.AgreementDate,
		CO.BookingDay,
		CO.Scheme,
		CO.Rate,
		CO.TotalDue,
		CO.RegistrationNo,
		CO.Source,
		CO.SourceDetails,
		CO.Bank,
		CO.PANNo1,
		CO.PANNo2,
		CO.PANNo3,
		CO.StampDuty,
		CO.RegistrationTax,
		CO.TDS,
		CO.GST,
		CO.Parking,
		CO.FloorRise,
		CO.PLC,
		CO.InfraCharge,
		CO.CorrespondenceAddress,
		PD.CarpetArea,
		PD.SaleableArea,
		PD.AttachedTerrace,
		PD.EnclosedBalcony,
		PD.ParkingSlot,
		CO.MaintenanceDeposit
	FROM
		CustomerOrder AS CO,
		Customer AS C,
		ProjectMaster AS PM,
		ProjectDetails AS PD,
		Employee AS E
	WHERE
		CO.ProjectId = PM.ProjectId
	AND 
		C.CustomerId = CO.CustomerId
	AND 
		CO.BuildingType = PD.BuildingType
	AND 
		((CO.BookingDate BETWEEN @BookingDateFrom AND @BookingDateTo) OR ( @BookingDateFrom IS NULL AND @BookingDateTo IS NULL))
	AND 
		PM.ProjectId = PD.ProjectId
	AND 
		CO.FlatNumber=PD.FlatNo
	AND 
		(CO.EmployeeId = E.EmployeeId OR CO.SalesAgentId= E.EmployeeId)
	AND 
		CO.IsActive !=0 
	AND
		(E.EmployeeId in (Select EmployeeId from Employee where FirstName+ ' ' +LastName like '%'+@Name+'%')
	OR
		E.EmployeeId in (Select EmployeeId from Employee where FirstName+ ' ' +LastName like '%'+@Name+'%' And ReportingTo in (Select EmployeeId from Employee where ReportingTo =@EntityId)))
END

GO

