/****** Object:  StoredProcedure [dbo].[Employee_PaymentStatus_Insert]    Script Date: 05-07-2018 19:32:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_PaymentStatus_Insert]
(
@OrderId int,
@BrokerageAmount float,
@InVoiceNo float,
@InvoiceDate date,
@PaymentMade float,
@AmountCr float,
@AmountDr float,
@ChequeNo varchar(30),
@DateOfCheque date
--@Id int
--@EntityId int
)
AS
BEGIN

	INSERT INTO ChannelPartnerPaymentStatus
(
		OrderId,
		BrokerageAmount,
		InVoiceNo,
		InvoiceDate,
		PaymentMade,
		AmountCr,
		AmountDr,
		ChequeNo,
		DateOfCheque,
		IsActive,
		CreatedOn
		--CreatedBy
	)
	VALUES 
	(	
		@OrderId,
		@BrokerageAmount,
		@InVoiceNo,
		@InvoiceDate,
		@PaymentMade,
		@AmountCr,
		@AmountDr,
		@ChequeNo,
		@DateOfCheque,
		1,
		GETDATE()
		--@Id
		--(Select FirstName from Employee as E where E.EmployeeId = @EntityId)
	)
END
GO

