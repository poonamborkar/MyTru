/****** Object:  StoredProcedure [dbo].[Employee_PaymentStatus_Select]    Script Date: 05-07-2018 19:33:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_PaymentStatus_Select]
(

@BrokerId int
 
)
AS
BEGIN
	Select 
		PS.OrderId,
		PS.BrokerageAmount,
		PS.InVoiceNo,
		PS.InvoiceDate,
		PS.PaymentMade,
		PS.AmountCr,
		PS.AmountDr,
		PS.ChequeNo,
		PS.DateOfCheque
	FROM
		ChannelPartnerPaymentStatus AS PS,
		ChannelPartnerMaster AS CP,
		CustomerOrder AS CO
		
	WHERE
		CO.BrokerId=CP.BrokerId
	AND
		CO.OrderId=PS.OrderId
	AND
		CP.BrokerId=@BrokerId
	AND
		CP.IsActive = 'True'
END
GO

