/****** Object:  StoredProcedure [dbo].[Employee_PaymentStatus_Update]    Script Date: 05-07-2018 19:33:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Employee_PaymentStatus_Update]
(
@OrderId int,
@BrokerageAmount float,
@InVoiceNo float,
@InvoiceDate date,
@PaymentMade float,
@AmountCr float,
@AmountDr float,
@ChequeNo varchar(30),
@DateOfCheque date
--@Id int
--@EntityId int,
--@UserType varchar(20)
)
AS
BEGIN
	UPDATE ChannelPartnerPaymentStatus
	SET 
		BrokerageAmount = @BrokerageAmount,
		InVoiceNo = @InVoiceNo,
		InvoiceDate = @InvoiceDate,
		PaymentMade = @PaymentMade,
		AmountCr = @AmountCr,
		AmountDr = @AmountDr,
		ChequeNo = @ChequeNo,
		DateOfCheque = @DateOfCheque,
		UpdatedOn = GETDATE()
		--UpdatedBy=(Select FirstName from Employee as E where E.EmployeeId = @EntityId)
		--UpdatedBy=(Select FirstName from UserMaster as UM where UM.EntityId = @EntityId AND UM.UserType = @UserType)
		--UpdatedBy=@Id
	WHERE
		OrderId= @OrderId
	AND
		IsActive = 1
END
GO

