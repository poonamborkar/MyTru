/****** Object:  StoredProcedure [dbo].[Employee_ProjectDetails_Insert]    Script Date: 05-07-2018 19:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Employee_ProjectDetails_Insert]    
(
	@ProjectName varchar(25),
	@BuildingType varchar(25),
	@Floor varchar(10),
	@LayoutType varchar(25),
	@BHK varchar(10),
	@FlatNo int,
	@ReraArea float,
	@Rate float,
	@CarpetArea float,
	@EnclosedBalcony varchar(20),
	@AttachedTerrace varchar(20),
	@ParkingSlot varchar(10),
	@ParkingCharge float,
	@PLC float,
	@InfraCharge float,
	@FloorRise float,
	@MonthlyMaintenance float
	--@Id int
)
AS
BEGIN 
INSERT INTO ProjectDetails 
(
	ProjectId,
	BuildingType,
	[Floor],
	LayoutType,
	BHK,
	FlatNo, 
	ReraArea,
	Rate,
	CarpetArea,
	EnclosedBalcony,
	AttachedTerrace,
	ParkingSlot,
    ParkingCharge,
    PLC,
    InfraCharge,
    FloorRise,
	MonthlyMaintenance,
	Available,
	IsActive,
	CreatedOn
	--CreatedBy
)
VALUES
(
	(SELECT ProjectId FROM ProjectMaster WHERE ProjectName=@ProjectName),
	@BuildingType,
	@Floor,
	@LayoutType,
	@BHK,
	@FlatNo, 
	@ReraArea, 
	@Rate,
	@CarpetArea,
	@EnclosedBalcony,
	@AttachedTerrace,
	@ParkingSlot,
	@ParkingCharge,
	@PLC,
	@InfraCharge,
	@FloorRise,
	@MonthlyMaintenance,
	'yes',
	1,
	GETDATE()
	--@Id
)
		--SET IDENTITY_INSERT ProjectDetails OFF 
END
GO

