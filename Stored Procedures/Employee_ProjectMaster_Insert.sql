/****** Object:  StoredProcedure [dbo].[Employee_ProjectMaster_Insert]    Script Date: 05-07-2018 19:34:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Employee_ProjectMaster_Insert]        
( 
     
	@ProjectName VARCHAR(30),
	@ProjectType VARCHAR(20),
	@ReraRegistrationNo varchar(20),  
	@ReraApproval VARCHAR(15),  
	--@ProjectImage varchar(20),  
	@TotalBuilding int,  
	@NoOfFlat int,
	--@Latitude FLOAT,
	--@Longitude FLOAT,
	@Address1 varchar(50),
	@Address2 varchar(50),
	@Landmark varchar(20),
	@Location varchar(20),
	@City varchar(20),
	@State varchar(20),
	@Country varchar(25),
	@PinCode int,
	@Rate Float
	--@Id int

--	@CreatedBy varchar(20)
)     
as         
Begin   
    Insert into ProjectMaster
	(ProjectName, 
	 ProjectType,
	 ReraRegistrationNo,
	 ReraApproval, 
	 ProjectImage, 
	 TotalBuilding, 
	 NoOfFlat,
	 Latitude,
	 Longitude,
	 Address1,
	 Address2,
	 Landmark,
	 Location,
	 City,
	 State,
	 Country, 
	 Pincode,
	 IsActive,
	 Rate,
     CreatedOn
    -- CreatedBy
	 )         
    Values 
	(
	@ProjectName, 
	@ProjectType,
	@ReraRegistrationNo, 
	@ReraApproval, 
	'Not Available', 
	@TotalBuilding, 
	@NoOfFlat, 
	'00',
	'00', 
	@Address1, 
	@Address2, 
	@Landmark, 
	@Location, 
	@City, 
	@State, 
	@Country ,
	@PinCode,
	1,
	@Rate,
	GETDATE()
	--@Id
	)  
	   --SET IDENTITY_INSERT ProjectMaster OFF 
End
GO

