/****** Object:  StoredProcedure [dbo].[Employee_ProjectStatus_Insert]    Script Date: 05-07-2018 19:35:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Employee_ProjectStatus_Insert]
(
--@ProjectId int,
@ProjectName varchar(30),
@BuildingType varchar(30),
@CurrentStatus float,
@EstimationCompletionDates date,
@PlannedActivity varchar(30),
@CurrentPrice bigint,
@PriceRevision float
--@Id int
)
as
begin

insert into ProjectStatus
(
ProjectId,
BuildingType,
CurrentStatus,
EstimationCompletionDates,
PlannedActivity,
CurrentPrice,
PriceRevision
--CreatedBy
)
values
(
--@ProjectId,
(Select ProjectId from ProjectMaster where ProjectName=@ProjectName),
@BuildingType,
@CurrentStatus ,
@EstimationCompletionDates ,
@PlannedActivity,
@CurrentPrice,
@PriceRevision
--@Id
)

end
GO

