/****** Object:  StoredProcedure [dbo].[Employee_ProjectStatus_Update]    Script Date: 05-07-2018 19:36:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Employee_ProjectStatus_Update]
(
@ProjectName varchar(30),
@BuildingType varchar(30),
@CurrentStatus float,
@EstimationCompletionDates date,
@PlannedActivity varchar(250),
@CurrentPrice bigint,
@PriceRevision float
--@Id int
)
as
begin
Update ProjectStatus
set 
CurrentStatus = @CurrentStatus,
EstimationCompletionDates=@EstimationCompletionDates,
PlannedActivity=@PlannedActivity,
CurrentPrice=@CurrentPrice,
PriceRevision = @PriceRevision
--UpdatedBy = @Id
where 
ProjectId=(Select ProjectId from ProjectMaster where ProjectName = @ProjectName)
AND
BuildingType=@BuildingType
end
GO

