/****** Object:  StoredProcedure [dbo].[Employee_ReferralDashboard_Select]    Script Date: 05-07-2018 19:37:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_ReferralDashboard_Select] 
(
	@FromDate Date =NULL,
	@ToDate Date=NULL,
	@Name Varchar(35)=NULL,
	@EntityId int,
	@UserType Varchar(30)
)
AS
BEGIN
	if (@Name='ALL')
		Exec Employee_ReferralDashboard_SelectAll @EntityId,@UserType
	Else
  SELECT 
		RS.ReferralId,
		RS.ReferralBy,
		RS.CrmAssinged,
		RS.BrokerAssinged,
		RS.IncentivePraposed,
		(R.FirstName +' '+ R.LastName) AS Name,
		ReferralStatus,
		CONVERT(date, R.CreatedOn) As CreatedOn,
		R.PreferredCity,
		RS.Source,
		RS.Face2Face,
		RS.NoOfSiteVisit,
		RS.SalesAgentAssinged
	FROM
		ReferralStatus As RS (nolock)
	Inner Join
		Referral As R (nolock)
	On 
		RS.ReferralId= R.ReferralId 
	inner join
		Employee AS E (nolock)
	On
		(RS.SalesAgentAssinged=E.EmployeeId OR RS.CrmAssinged=E.EmployeeId )
		AND ((R.CreatedOn >= @FromDate AND R.CreatedOn <= @ToDate) OR (@FromDate IS NULL AND @ToDate is NULL ))	
	WHERE
		RS.IsActive !=0 
		AND
		E.EmployeeId = (Select EmployeeId from Employee where FirstName+ ' ' +LastName like '%'+@Name+'%')
		OR
		E.EmployeeId =(Select EmployeeId from Employee where FirstName+ ' ' +LastName like '%'+@Name+'%' And ReportingTo in (Select EmployeeId from Employee where ReportingTo =@EntityId))
END

GO

