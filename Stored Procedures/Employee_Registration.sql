/****** Object:  StoredProcedure [dbo].[Employee_Registration]    Script Date: 13-Jul-18 12:37:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Employee_Registration]
(
	@EntityId int,
	@UserType Varchar(30),
	@FirstName varchar(50),				--Customer Parameters
	@LastName varchar(50),
	@MobileNo  Varchar(12),
	@Email Varchar(50),
	@AddressLine1 varchar(50),
	@City Varchar(50),					
	@Dob Date = NULL,
	@Role Varchar(25)=NULL,
	@AssingedArea Varchar(25)=NULL,
	@AssingedProject1 Varchar(30)=NULL,
	@AssingedProject2 Varchar(30)=NULL,
	@AssingedProject3 Varchar(30)=NULL,
	@ReportingTo Varchar(30)=NULL,
	@Level Varchar(30)=NULL
)
As 
BEGIN
declare @AutoGenPswd varchar(20);
 SET @AutoGenPswd=(SELECT SUBSTRING(CONVERT(varchar(40), NEWID()),1,6));

	Insert into 
		Employee
		(
			FirstName,
			LastName,
			MobileNo,
			Email,
			StreetAddress1,
			City,
			IsActive,
			Dob,
			CreatedOn,
			Role,
			AssingedArea,
			AssingedProject1,
			AssingedProject2,
			AssingedProject3,
			ReportingTo,
			Level
			)
	Values
		(
			@FirstName,
			@LastName,
			@MobileNo,
			@Email,
			@AddressLine1,
			@City,
			0,
			@Dob,
			GetDate(),
			@Role,
			@AssingedArea,
			@AssingedProject1,
			@AssingedProject2,
			@AssingedProject3,
			@ReportingTo,
			@Level
		)
	Insert into 
	UserMaster 
		(
			FirstName, 
			LastName,
			UserId,
			Pwd,
			EncryptPass,
			UserType,
			MobileNo,
			IsActive,
			EntityId,
			CreatedOn
		) 
	Values 
		(
			@FirstName,
			@LastName,
			@Email, 
			'',
			EncryptByPassPhrase('VovTru',CONVERT(varbinary(500),@AutoGenPswd)), 
			'Employee',
			@MobileNo,
			'true',
			(
	SELECT TOP 
		1 EmployeeId 
	from 
		Employee as E
	Where
			 E.Email = @Email And
			 E.FirstName = @FirstName And 
			 E.LastName = @LastName And 
			 E.MobileNo = @MobileNo
	Order By 
		EmployeeId 
	DESC
			),
			GETDATE()
	) 

END

	Select 
		@FirstName+' '+@LastName As EmployeeName,
		@Email As EmpEmail ,
		(select FirstName+' '+LastName From Employee Where EmployeeId=@EntityId) As AdminName,
		(select Email From Employee Where EmployeeId=@EntityId) As AdminEmail,
		convert(varchar(100),DecryptByPassPhrase('VovTru', EncryptPass )) As Pwd
	From
		UserMaster
	Where 
		UserId=@Email

		
GO

