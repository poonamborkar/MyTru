/****** Object:  StoredProcedure [dbo].[Employee_ReportingTo_Select]    Script Date: 05-07-2018 19:37:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Employee_ReportingTo_Select] --17,'Employee'
(
	@EntityId Int,
	@UserType Varchar
)
AS
BEGIN
	SELECT 
		FirstName+' '+LastName AS Name,
		EmployeeId As ID
	From 
		Employee
	Where
		EmployeeId=@EntityId 
		OR
		EmployeeId in (Select EmployeeId from Employee where ReportingTo = @EntityId ) 
		OR
		EmployeeId in (Select EmployeeId from Employee where ReportingTo in (Select EmployeeId from Employee where ReportingTo = @EntityId))
		
		
		
END

GO

