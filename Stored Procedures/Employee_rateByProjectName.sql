/****** Object:  StoredProcedure [dbo].[Employee_rateByProjectName]    Script Date: 05-07-2018 19:36:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Employee_rateByProjectName]
(
	@ProjectName varchar(25)
)
AS
	declare @ProId int;
	set @ProId=(Select ProjectId from ProjectMaster where ProjectName=@ProjectName)
	print @ProId;
BEGIN
SELECT 
	Rate 
FROM
	ProjectMaster 
where
	ProjectId=@ProId
END
GO

