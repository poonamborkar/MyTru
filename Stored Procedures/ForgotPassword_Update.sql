/****** Object:  StoredProcedure [dbo].[ForgotPassword_Update]    Script Date: 05-07-2018 19:38:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ForgotPassword_Update]
(
	@Search varchar(50),
	@NewPassword varchar(20)
)
AS
BEGIN
	  UPDATE UserMaster
	  SET
		EncryptPass= EncryptByPassPhrase('VovTru',CONVERT(varbinary(max),@NewPassword))
	  WHERE 
		 (UserId=@Search or MobileNo=@Search)	 
END
GO

