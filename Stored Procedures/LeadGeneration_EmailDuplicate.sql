/****** Object:  StoredProcedure [dbo].[LeadGeneration_EmailDuplicate]    Script Date: 05-07-2018 19:38:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[LeadGeneration_EmailDuplicate] --'Prajwal@gmail.com'
(
	@EmailId varchar(30)	
)
As
Begin
	IF EXISTS (SELECT Referral.Email FROM Referral where Email = @EmailId)
		BEGIN
			Select 'Email Id already exists. Please enter other email Id' as IsAvailable
		END
	Else
		BEgin
			Select 'Available' as IsAvailable
		End
End

GO

