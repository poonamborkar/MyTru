/****** Object:  StoredProcedure [dbo].[LeadGeneration_MobileDuplicate]    Script Date: 05-07-2018 19:39:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[LeadGeneration_MobileDuplicate]
(
	@MobileNo varchar(30)	
)
As
Begin
	IF EXISTS (SELECT Referral.MobileNo FROM Referral where MobileNo = @MobileNo)
		BEGIN
			Select 'This Mobile number is already registered' as IsAvailable
		END
	Else
		Begin
			Select 'Available' as IsAvailable
		End
End

GO

