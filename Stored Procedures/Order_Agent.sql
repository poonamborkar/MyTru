/****** Object:  StoredProcedure [dbo].[Order_Agent]    Script Date: 05-07-2018 19:39:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Order_Agent]
(
@EmployeeId int
)
AS
BEGIN
Select CO.OrderId from CustomerOrder AS CO,
Employee AS E
WHERE
E.EmployeeId=@EmployeeId
AND
CO.EmployeeId=E.EmployeeId
END
GO

