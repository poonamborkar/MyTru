/****** Object:  StoredProcedure [dbo].[Order_CRMAgent]    Script Date: 05-07-2018 19:40:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Order_CRMAgent]
(
@EmployeeId int
)
AS
BEGIN
Select CO.OrderId from CustomerOrder AS CO,
Employee AS E
WHERE
E.EmployeeId=@EmployeeId
AND
CO.EmployeeId=E.EmployeeId
AND
E.Role='CRM Agent'
END
GO

