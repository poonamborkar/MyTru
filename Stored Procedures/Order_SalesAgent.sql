/****** Object:  StoredProcedure [dbo].[Order_SalesAgent]    Script Date: 05-07-2018 19:40:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Order_SalesAgent]
(
@EmployeeId int
)
AS
BEGIN
Select CO.OrderId from CustomerOrder AS CO,
Employee AS E
WHERE
CO.SalesAgentId=@EmployeeId
AND
CO.SalesAgentId=CO.EmployeeId
AND
E.Role='Sales Agent'
END
GO

