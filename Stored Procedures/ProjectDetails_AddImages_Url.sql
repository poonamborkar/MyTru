/****** Object:  StoredProcedure [dbo].[ProjectDetails_AddImages_Url]    Script Date: 05-07-2018 19:40:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_AddImages_Url] --'https://truimages.blob.core.windows.net/projectstatus/PuneVille_1.jpg'
(
	@ImageUrl varchar(max)=NULL,
	@ProjectName Varchar(50)=NULL,
	@BuildingType Varchar(50)=NULL,
	@IssueId Int=NULL
)
AS
BEGIN

	Insert Into
		Tru_Images
	(	
		ImageUrl,
		ProjectName,
		BuildingType,
		IssueId
	)
	Values
	(
		@imageUrl,
		@ProjectName,
		@BuildingType,
		@IssueId
	)
		
END
GO

