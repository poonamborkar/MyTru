/****** Object:  StoredProcedure [dbo].[ProjectDetails_BHK_Select]    Script Date: 05-07-2018 19:41:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_BHK_Select]
(
	@ProjectName varchar(50)
)
AS
BEGIN
SELECT
	Distinct(BHK)
FROM 
	ProjectDetails PD, 
	ProjectMaster PM
WHERE
	PD.ProjectId = PM.ProjectId
AND 
	PM.ProjectName = @ProjectName 
END
GO

