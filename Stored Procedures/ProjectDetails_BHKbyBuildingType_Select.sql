/****** Object:  StoredProcedure [dbo].[ProjectDetails_BHKbyBuildingType_Select]    Script Date: 05-07-2018 19:41:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_BHKbyBuildingType_Select]

(
@BuildingType varchar(15),
@ProjectName varchar(25) 
)
AS
BEGIN
select distinct (PD.BHK) from ProjectDetails AS PD,
ProjectMaster AS PM
where
PM.ProjectId=PD.ProjectId
AND
PD.BuildingType = @BuildingType
AND
PM.ProjectName=@ProjectName
END
GO

