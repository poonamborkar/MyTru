/****** Object:  StoredProcedure [dbo].[ProjectDetails_BuildingType_Select]    Script Date: 05-07-2018 19:42:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_BuildingType_Select]
(
	@ProjectName varchar(30)
)
AS
BEGIN
SELECT
	Distinct(BuildingType)
FROM 
	ProjectDetails PD, 
	ProjectMaster PM
WHERE
	PD.ProjectId = PM.ProjectId
AND 
	PM.ProjectName = @ProjectName
end
GO

