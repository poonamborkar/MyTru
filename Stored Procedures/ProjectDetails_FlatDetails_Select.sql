/****** Object:  StoredProcedure [dbo].[ProjectDetails_FlatDetails_Select]    Script Date: 05-07-2018 19:42:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_FlatDetails_Select] 
@ProjectName varchar(30),
@BuildingType varchar(50),
@BHK varchar(20)= null,
@LayoutType varchar(50)= null,
@FlatNo varchar(20)

AS
BEGIN

	SELECT
		PD.CarpetArea, 
		PD.EnclosedBalcony, 
		PD.AttachedTerrace, 
		PD.ParkingSlot,
		PD.Rate,		
		(((PD.ReraArea * PD.Rate)) + PD.PLC + PD.InfraCharge + PD.ParkingCharge + PD.FloorRise)  AS SubTotal,
		PD.FloorRise,
		(PD.ReraArea * PD.Rate) As AgreedCost,
		PD.FlatView,
		PD.ReraArea,
		PD.ParkingCharge,
		PD.PLC,
		PD.InfraCharge,
		(PD.MonthlyMaintenance * CarpetArea) AS MonthlyMaintenance
	FROM 
		ProjectDetails AS PD,
		ProjectMaster AS PM
	WHERE
		PM.ProjectId=PD.ProjectId
	AND
		PM.ProjectName=@ProjectName
	AND
		PD.BuildingType=@BuildingType
	AND
		(PD.BHK = @BHK
	OR
		@BHK is  NULL )
	AND
		(PD.LayoutType=@LayoutType 
	OR
		@LayoutType is NULL)
	AND
		PD.FlatNo=@FlatNo
	AND
		PD.Available = 'Yes'
END

GO

