/****** Object:  StoredProcedure [dbo].[ProjectDetails_FlatNo_Select]    Script Date: 05-07-2018 19:42:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ProjectDetails_FlatNo_Select] --'Manav Swapnalok','t1','',''
(
	@ProjectName varchar(20),
	@BuildingType varchar(20),
	@BHK varchar(20)=NULL,
	@LayoutType varchar(20)=Null
)
AS
BEGIN
	SELECT
		Distinct(FlatNo)
	FROM 
			ProjectDetails PD,
			ProjectMaster PM 
	WHERE
		PD.ProjectId = PM.ProjectId
	AND 
		PM.ProjectName=@ProjectName
	AND 
		PD.BuildingType=@BuildingType 
	AND 
		(PD.BHK = @BHK OR @BHK is Null) 
	And 
		(PD.LayoutType=@LayoutType OR @LayoutType is NULL)
	And 
		PD.Available like 'Yes%'
END
GO

