/****** Object:  StoredProcedure [dbo].[ProjectDetails_Insert]    Script Date: 05-07-2018 19:43:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ProjectDetails_Insert]    
(
@ProjectId int,
@BuildingType varchar(25),
@Floor varchar(10),
@LayoutType varchar(25),
@BHK varchar(10),
@FlatNo int,
@Available varchar(20),
@ReraArea float,
@Rate float
--@Id int
)
as
BEGIN 
insert into ProjectDetails 
(
ProjectId,BuildingType,Floor,LayoutType,BHK,FlatNo, 
Available, ReraArea, Rate,IsActive,CreatedOn
--CreatedBy
)
values
(
@ProjectId,@BuildingType,@Floor,@LayoutType,@BHK,@FlatNo, 
@Available, @ReraArea, @Rate,1,GETDATE()--,@Id
)
SELECT PM.ProjectId,PM.ProjectName,PD.FlatId
FROM ProjectMaster PM
INNER JOIN ProjectDetails PD ON PD.ProjectId = PM.ProjectId 
END
GO

