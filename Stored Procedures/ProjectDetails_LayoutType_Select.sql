/****** Object:  StoredProcedure [dbo].[ProjectDetails_LayoutType_Select]    Script Date: 05-07-2018 19:43:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_LayoutType_Select]
(
	@BuildingType varchar(20),
	@BHK varchar(20)= null,
	@ProjectName varchar(20)
)
AS
BEGIN

Select Distinct(PD.LayoutType) from ProjectDetails AS PD,
ProjectMaster AS PM
WHERE

	PM.ProjectId = PD.ProjectId
	AND
	PM.ProjectName = @ProjectName
	AND
	PD.BuildingType = @BuildingType
	AND
	PD.BHK = @BHK
	OR
	@BHK =  NULL 

END
GO

