/****** Object:  StoredProcedure [dbo].[ProjectDetails_Location_SelectAll]    Script Date: 05-07-2018 19:43:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectDetails_Location_SelectAll]

AS
BEGIN

SELECT 
Distinct(Location) 
FROM
ProjectMaster

END
GO

