/****** Object:  StoredProcedure [dbo].[ProjectDetails_ProjectName_Select]    Script Date: 05-07-2018 19:44:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ProjectDetails_ProjectName_Select]
(
	@Location varchar(50)
)
AS
BEGIN
SELECT
	ProjectName 
FROM
	ProjectMaster 
WHERE
	[Location] = @Location
END
GO

