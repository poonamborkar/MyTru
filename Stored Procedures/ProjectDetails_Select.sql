/****** Object:  StoredProcedure [dbo].[ProjectDetails_Select]    Script Date: 05-07-2018 19:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[ProjectDetails_Select] -- 'Puneville Phase 1','Punawale', '2 BHK'   
(

	@ProjectName varchar(20),
	@Location varchar (20),
	@BHK varchar(20)
)
AS
BEGIN

SELECT Distinct
	((Select count(flatId) From ProjectDetails where BuildingType = PD.BuildingType and Available like 'No%')*100)/(Select count(flatId) From ProjectDetails where BuildingType = PD.BuildingType ) As Result,
	PD.BuildingType, 
	PD.Floor,
	PD.LayoutType, 
	PD.ReraArea, 
	PD.FlatNo, 
	PD.Available,
	PD.BHK 
from ProjectMaster as PM,
	ProjectDetails as PD
where 
	PM.ProjectId=PD.ProjectId
And
	PM.ProjectName=@ProjectName 
And 
	PM.Location = @Location
And 
	PD.BHK = @BHK
And
	PD.IsActive='TRUE'                             
END


 


GO

