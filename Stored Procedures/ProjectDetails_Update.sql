/****** Object:  StoredProcedure [dbo].[ProjectDetails_Update]    Script Date: 05-07-2018 19:44:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[ProjectDetails_Update]    
(
@FlatId int,
@ProjectId int,
@BuildingType varchar(25),
@Floor varchar(10),
@LayoutType varchar(25),
@BHK varchar(10),
@FlatNo int,
@Available varchar(20),
@ReraArea float,
@Rate float
--@Id int
)
as 
Begin
UPDATE ProjectDetails             --Set Values For Update
set
ProjectId=@ProjectId,
BuildingType=@BuildingType ,
Floor=@Floor,
LayoutType=@LayoutType,
BHK=@BHK,
FlatNo=@FlatNo,
Available=@Available,
ReraArea=@ReraArea,
Rate=@Rate,
UpdatedOn=GETDATE()
--UpdatedBy=@Id
where FlatId=@FlatId        --Update Project Details By Using ProjectId
and IsActive=1 
End
GO

