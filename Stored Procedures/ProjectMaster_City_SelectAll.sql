/****** Object:  StoredProcedure [dbo].[ProjectMaster_City_SelectAll]    Script Date: 05-07-2018 19:45:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectMaster_City_SelectAll]
AS
BEGIN

Select 
	City
FROM
	ProjectMaster
WHERE 
	IsActive = 'True'
END
GO

