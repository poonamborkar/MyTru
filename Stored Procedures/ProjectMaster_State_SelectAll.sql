/****** Object:  StoredProcedure [dbo].[ProjectMaster_State_SelectAll]    Script Date: 05-07-2018 19:45:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProjectMaster_State_SelectAll]
AS
BEGIN

Select 
	State
FROM
	ProjectMaster
WHERE 
	IsActive = 'True'
END
GO

