/****** Object:  StoredProcedure [dbo].[Referral_SiteVisit_To_Order]    Script Date: 05-07-2018 19:46:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Referral_SiteVisit_To_Order]
(
	@ToDate Date=NULL,
	@FromDate Date=NULL
)
As 
BEGIN
	Declare @SiteVisit float;
	Declare @NoOfOrder float;
	
	Set @NoOfOrder=	(
					Select COUNT (OrderId)
					From 
						CustomerOrder As CO,
						ReferralStatus As RS
					where RS.NoOfSiteVisit > 0 
						AND ( CO.BookingDate >=@FromDate AND  CO.BookingDate <= @ToDate) 
						AND RS.ReferralId=Co.CustomerId
					)
					
	Set	@SiteVisit= (
					Select COUNT (NoOfSiteVisit)
					From 
						ReferralStatus As RS
					where RS.NoOfSiteVisit > 0 
						AND ( RS.SiteVisitedOn >= @FromDate AND RS.SiteVisitedOn  <= @ToDate) 
					)				
		If(@SiteVisit=0)
Begin
			Select ( 'No Site Visit Done')
END
		else
BEGIN					
			Select (@NoOfOrder/@SiteVisit)*100 As Ratio
END			
END

GO

