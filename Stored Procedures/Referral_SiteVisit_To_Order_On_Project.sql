/****** Object:  StoredProcedure [dbo].[Referral_SiteVisit_To_Order_On_Project]    Script Date: 05-07-2018 19:46:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Referral_SiteVisit_To_Order_On_Project]
(
	@ToDate Date=NULL,
	@FromDate Date=NULL,
	@ProjectName Varchar(30)=NULL
)
As 
BEGIN
	Declare @SiteVisit float;
	Declare @NoOfOrder float;
	
Set @NoOfOrder=	(
					Select COUNT (OrderId)
					From 
					CustomerOrder As CO,
					ReferralStatus As RS,
					ProjectMaster AS PM
					where RS.NoOfSiteVisit > 0 
					AND ( CO.BookingDate >=@FromDate AND  CO.BookingDate <= @ToDate) 
					AND RS.ReferralId=Co.CustomerId
					AND PM.ProjectName=@ProjectName
					AND PM.ProjectId = CO.ProjectId
				)
					
Set	@SiteVisit= (
					Select COUNT (NoOfSiteVisit)
					From 
					ReferralStatus As RS
					where RS.NoOfSiteVisit > 0 
					AND ( RS.SiteVisitedOn >=@FromDate AND RS.SiteVisitedOn  <= @ToDate) 
					AND RS.ProjectVisited=@ProjectName
				)
					
	If(@SiteVisit=0)
begin
	Select ( 'No Site Visit Done')
end
	else
begin
	Select (@NoOfOrder/@SiteVisit)*100 As Ratio
end
END

GO

