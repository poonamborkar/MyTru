/****** Object:  StoredProcedure [dbo].[Referral_To_SiteVisit]    Script Date: 05-07-2018 19:46:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Referral_To_SiteVisit]
(
	@ToDate Date,
	@FromDate Date
)
As 
BEGIN
	Declare @SiteVisit float;
	Declare @NoOfReferral float;
	Set	@SiteVisit= 
				(
					Select COUNT (NoOfSiteVisit)
					From ReferralStatus
					where NoOfSiteVisit > 0 
					AND ( SiteVisitedOn >= @FromDate AND  SiteVisitedOn <= @ToDate) 
				) 
			
	Set @NoOfReferral=
				(
					Select COUNT (R.ReferralId)
					From Referral As R,
					ReferralStatus As RS
					Where 
					 RS.NoOfSiteVisit > 0 AND
					(( CONVERT(date, R.CreatedOn)) >= @FromDate AND  ( CONVERT(date, R.CreatedOn)) <= @ToDate) 
					AND R.IsActive=1
					AND RS.ReferralId=R.ReferralId
				)

	If(@NoOfReferral=0)
begin
	Select ( 'No Site Visit Done')
end
	else
begin					
	Select (@SiteVisit/@NoOfReferral)*100 As Ratio
END		
END

GO

