/****** Object:  StoredProcedure [dbo].[Referral_To_SiteVisit_OnProject]    Script Date: 13-Jul-18 10:29:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Referral_To_SiteVisit_OnProject]
(
	
	@FromDate Date=null,
	@ToDate Date=null,
	@ProjectName Varchar(30)=NULL
)
As 
BEGIN
	Declare @SiteVisit float;
	Declare @NoOfReferral float;
	Set	@SiteVisit= 
				(
					Select COUNT (NoOfSiteVisit)
					From ReferralStatus as RS,
					 ProjectMaster as PM
					where 
					NoOfSiteVisit > 0 
					AND PM.ProjectName = RS.ProjectVisited
					AND 
					(RS.SiteVisitedOn >= @FromDate AND  RS.SiteVisitedOn <= @ToDate)
					or (ProjectVisited = @ProjectName )
				) 		
	Set @NoOfReferral=
				(
					Select COUNT (R.ReferralId)
					From Referral AS R,
					ReferralStatus As RS
					Where 
					RS.NoOfSiteVisit > 0  AND
					((( CONVERT(date, R.CreatedOn)) >= @FromDate AND  ( CONVERT(date, R.CreatedOn)) <= @ToDate) OR (@FromDate=NULL AND @ToDate = NULL))
					AND R.IsActive=1
					AND RS.ReferralId=R.ReferralId
				)

	If(@NoOfReferral=0)
begin
	Select ( 'No Site Visit Done') As Ratio
end
	else
begin					
	Select (@SiteVisit/@NoOfReferral) As Ratio
END		
END

GO

