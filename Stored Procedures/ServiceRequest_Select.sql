/****** Object:  StoredProcedure [dbo].[ServiceRequest_Select]    Script Date: 05-07-2018 19:48:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ServiceRequest_Select]
(
@CustomerId INT
)
AS 
BEGIN 
SELECT
CustomerId,
ProjectId,
EmployeeId,
SalesAgentId,
OrderId,
BrokerId,
ProjectName,
Building,
Flat,
IssueStatus,
IssueSubject,
IssueType,
Details,
CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
IsActive
FROM ServiceRequest
WHERE CustomerId=@CustomerId
AND IsActive='True'
end
GO

