/****** Object:  StoredProcedure [dbo].[Service_Search]    Script Date: 05-07-2018 19:47:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Service_Search] 
(	
	@SearchInput varchar(30)
) 
AS
BEGIN  
	select distinct 
				IssueId,
				EnteredById,
				ProjectName,
				Building,
				Flat,
				IssueSubject,
				IssueStatus,
				IssueType,
				Details,
				CONVERT(NVARCHAR,IssueDate,106) AS [IssueDate],
				IsActive from Customer_ServiceRequest
	Where (IssueStatus !='Closed')
	   AND (ProjectName LIKE @SearchInput+'%' 
	   OR IssueId LIKE @SearchInput+'%' 
	   OR IssueType LIKE @SearchInput+'%');
	  
END
GO

