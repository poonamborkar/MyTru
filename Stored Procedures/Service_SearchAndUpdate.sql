/****** Object:  StoredProcedure [dbo].[Service_SearchAndUpdate]    Script Date: 05-07-2018 19:48:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Service_SearchAndUpdate] 
(	
	@IssueStatus Varchar(50),
	@AddDetails varchar(100),
	@IssueId int,
	@UserType varchar(20),
	@EntityId int
	--@Id int
) 
AS
	declare @UpdateOnDate varchar(20);
	Set @UpdateOnDate=  convert(varchar, getdate(), 1);

BEGIN  
	   UPDATE Customer_ServiceRequest
		SET IssueStatus=@IssueStatus,Details = Customer_ServiceRequest.Details+','+ CHAR(13) +@AddDetails+'(EnteredBy:'+(select FirstName+' '+LastName from UserMaster where UserType=@UserType and EntityId=@EntityId)+',EnteredOn:'+@UpdateOnDate+')'
		--UpdatedBy=@Id
		WHERE IssueId=@IssueId;



	  
END
GO

