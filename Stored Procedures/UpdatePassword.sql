/****** Object:  StoredProcedure [dbo].[UpdatePassword]    Script Date: 05-07-2018 19:48:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[UpdatePassword]
(
	@UserId varchar(30),
	@Pwd varchar(15),
	@Newpwd varchar(15)	
)
AS 
begin

UPDATE UserMaster
	  SET
		EncryptPass= EncryptByPassPhrase('VovTru',CONVERT(varbinary(max),@Newpwd))
	  WHERE
	   
		 (UserId=@UserId and DecryptByPassPhrase('VovTru',EncryptPass)=@Pwd)
	And
		 EXISTS (SELECT
		UserId,
		EncryptPass
	FROM 
		UserMaster 
	WHERE 
		UserId=@UserId
	AND 
		DecryptByPassPhrase('VovTru',EncryptPass)=@Pwd)


	--  UPDATE UserMaster
	--  SET
	--	EncryptPass= EncryptByPassPhrase('VovTru',CONVERT(varbinary(max),@NewPwd))
	--  WHERE
	--	 (UserId=@UserId and DecryptByPassPhrase('VovTru',EncryptPass)=@Pwd)	
	--Select 'Successfully Change' 

END


--AS
--IF EXISTS (SELECT
--		UserId,
--		EncryptPass
--	FROM 
--		UserMaster 
--	WHERE 
--		UserId=@UserId 
--	AND 
--		DecryptByPassPhrase('VovTru',EncryptPass)=@Pwd ) 
--BEGIN
-- UPDATE UserMaster
-- SET
--	EncryptPass= EncryptByPassPhrase('VovTru',CONVERT(varbinary(max),@NewPwd))
--WHERE
--	(UserId=@UserId and EncryptPass=@Pwd)	
--	print 'Successfully Change' 
--END


 --UPDATE UserMaster
	--  SET
	--	EncryptPass= EncryptByPassPhrase('VovTru',CONVERT(varbinary(max),1235))
	--  WHERE
	   
	--	 (UserId='ankush@gmail.com' and DecryptByPassPhrase('VovTru',EncryptPass)='1235')
	--And
	--	 EXISTS (SELECT
	--	UserId,
	--	EncryptPass
	--FROM 
	--	UserMaster 
	--WHERE 
	--	UserId='ankush@gmail.com' 
	--AND 
	--	DecryptByPassPhrase('VovTru',EncryptPass)='1235')

	
	


		
GO

