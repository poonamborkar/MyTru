/****** Object:  StoredProcedure [dbo].[UserMaster_ALL_Password]    Script Date: 05-07-2018 19:50:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UserMaster_ALL_Password] 
(
@Search Varchar(30) 
)
AS
BEGIN
	Select 
FirstName,
LastName,
UserId,
convert(varchar(100),DecryptByPassPhrase('VovTru', EncryptPass )) As Password,
UserType
from UserMaster 
Where UserId Like '%'+@Search+'%'
OR
UserType Like '%'+@Search+'%'
OR
FirstName+LastName like '%'+@Search+'%'
END



GO

