/****** Object:  StoredProcedure [dbo].[User_Registration_EmailDuplicate]    Script Date: 05-07-2018 19:49:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[User_Registration_EmailDuplicate]
(
	@EmailId varchar(30)	
)
As
Begin
	IF EXISTS (SELECT UserMaster.UserId FROM UserMaster where UserId = @EmailId)
		BEGIN
			Select 'Email Id already exists. Please enter other email Id' as IsAvailable
		END
	Else
		BEgin
			Select 'Available' as IsAvailable
		End
End

GO

