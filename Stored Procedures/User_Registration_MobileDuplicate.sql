/****** Object:  StoredProcedure [dbo].[User_Registration_MobileDuplicate]    Script Date: 05-07-2018 19:49:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[User_Registration_MobileDuplicate]
(
	@MobileNo varchar(30)	
)
As
Begin
	IF EXISTS (SELECT UserMaster.MobileNo FROM UserMaster where MobileNo = @MobileNo)
		BEGIN
			Select 'This Mobile number is already registered' as IsAvailable
		END
	Else
		Begin
			Select 'Available' as IsAvailable
		End
End

GO

