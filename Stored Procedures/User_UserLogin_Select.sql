/****** Object:  StoredProcedure [dbo].[User_UserLogin_Select]    Script Date: 05-07-2018 19:50:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[User_UserLogin_Select]
(
 @UserId varchar(50),
 @Password Varchar(50)
)
AS
BEGIN
SELECT
	Id as UserId,
	UserId as UserEmail,
	UserType,
	EntityId,
	Firstname,
	LastName
	FROM [dbo].[UserMaster] 
	WHERE 
	--UserId=@UserId
	--AND Pwd=@Password
	--AND IsActive='true'
	UserId=@UserId AND IsActive=1 AND convert(varchar(100),DecryptByPassPhrase('VovTru', EncryptPass )) = @Password
	
END
GO

