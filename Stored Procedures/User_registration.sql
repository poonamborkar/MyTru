/****** Object:  StoredProcedure [dbo].[User_registration]    Script Date: 05-07-2018 19:49:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[User_registration]
(
	@Pwd Varchar(50),
	@UserType varchar(50),
	@Flag varchar(50) = null,
	@FirstName varchar(50),				--Customer Parameters
	@LastName varchar(50),
	@MobileNo  Varchar(12),
	@Email Varchar(50),
	@AddressLine1 varchar(50),
	@City Varchar(50),					
	@PreferredCity varchar(50) = NULL,			--Referral Parameters
	@PreferredLocation varchar(50)= NULL,
	@Budget Varchar(30)= NULL,
	@Dob Date = NULL,
	@ReraNo varchar(30) = NULL,
	@Organization varchar(50) = NULL,
	@PanNo varchar(15) = NULL,
	@AadharNo varchar(15) = NULL,
	@Type varchar(25) = NULL
)
AS
Begin

	IF  (@UserType = 'Customer' AND @Flag = 'Yes')
BEGIN
	
	Insert into
		Customer 
		(
			FirstName, 
			LastName, 
			MobileNo, 
			Email,
			AddressLine1, 
			City,
			IsActive,
			Dob,
			CreatedOn
		)         
    Values 
		(
			@FirstName, 
			@LastName, 
			@MobileNo, 
			@Email,
			@AddressLine1, 
			@City,
			'True',
			@Dob,
			GETDATE()			
		)      

	Insert into 
		UserMaster 
		(
			FirstName, 
			LastName,
			UserId,
			Pwd,
			EncryptPass,
			UserType,
			MobileNo,
			IsActive,
			EntityId,
			CreatedOn
		) 
	Values 
		(
			@FirstName, 
			@LastName,
			@Email, 
			'',
			EncryptByPassPhrase('VovTru',CONVERT(varbinary(500),@Pwd)), 
			@UserType,
			@MobileNo,
			'true',

			(
	SELECT TOP 
			1 CustomerId 
	from 
			Customer as C 
	Where 
			C.Email = @Email And 
			C.FirstName = @FirstName And 
			C.LastName = @LastName And 
			C.MobileNo = @MobileNo
	Order By 
			CustomerId 
	DESC
			),
			GetDate()
		) 
	

END
 ELSE IF (@UserType='Customer' AND @Flag = 'No')
BEGIN
	Insert into 
		Referral
		(
			FirstName,
			LastName,
			MobileNo,
			Email,
			AddressLine1,
			City,
			PreferredCity,
			PreferredLocation,
			Budget,
			IsActive,
			ReferredById,	
			ReferredByType,		
			Dob,
			CreatedOn		
		)
	Values
		(
			@FirstName,
			@LastName,
			@MobileNo,
			@Email,
			@AddressLine1,
			@City,
			@PreferredCity,
			@PreferredLocation,
			@Budget,
			'true',
			'0',
			'self',
			@Dob,
			GetDate()
		)
	Insert into 
	UserMaster 
		(
			FirstName, 
			LastName,
			UserId,
			Pwd,
			EncryptPass,
			UserType,
			MobileNo,
			IsActive,
			EntityId,
			CreatedOn
		) 
	Values 
		(
			@FirstName,
			@LastName,
			@Email, 
			'',
			EncryptByPassPhrase('VovTru',CONVERT(varbinary(500),@Pwd)), 
			'Referral',
			@MobileNo,
			'true',
			(
	SELECT TOP 
				1 ReferralId 
	from 
				Referral as R 
	Where
				R.Email = @Email And
				R.FirstName = @FirstName And 
				R.LastName = @LastName And 
				R.MobileNo = @MobileNo
	Order By 
				ReferralId 
	DESC
			),
			GETDATE()
	) 
		
END
ELSE IF (@UserType='Employee' OR @Flag=NULL)
BEGIN
	Insert into 
		Employee
		(
			FirstName,
			LastName,
			MobileNo,
			Email,
			StreetAddress1,
			City,
			IsActive,
			Dob,
			CreatedOn
			)
	Values
		(
			@FirstName,
			@LastName,
			@MobileNo,
			@Email,
			@AddressLine1,
			@City,
			'true',
			@Dob,
			GetDate()
		)
	Insert into 
	UserMaster 
		(
			FirstName, 
			LastName,
			UserId,
			Pwd,
			EncryptPass,
			UserType,
			MobileNo,
			IsActive,
			EntityId,
			CreatedOn
		) 
	Values 
		(
			@FirstName,
			@LastName,
			@Email, 
			'',
			EncryptByPassPhrase('VovTru',CONVERT(varbinary(500),@Pwd)), 
			'Employee',
			@MobileNo,
			'true',
			(
	SELECT TOP 
		1 EmployeeId 
	from 
		Employee as E
	Where
			 E.Email = @Email And
			 E.FirstName = @FirstName And 
			 E.LastName = @LastName And 
			 E.MobileNo = @MobileNo
	Order By 
		EmployeeId 
	DESC
			),
			GETDATE()
		) 
END
ELSE IF (@UserType='ChannelPartner' OR @Flag=NULL)
BEGIN
	Insert into 
		ChannelPartnerMaster
		(
			FirstName,
			LastName,
			MobileNo,
			Email,
			AddressLine1,
			City,
			IsActive,
			ReraNo,
			Organization,
			PanNo,
			AadharNo,
			Type,
			CreatedOn
			)
	Values
		(
			@FirstName,
			@LastName,
			@MobileNo,
			@Email,
			@AddressLine1,
			@City,
			'true',
			@ReraNo,
			@Organization,
			@PanNo,
			@AadharNo,
			@Type,
			GetDate()
		)
	Insert into 
	UserMaster 
		(
			FirstName, 
			LastName,
			UserId,
			Pwd,
			EncryptPass,
			UserType,
			MobileNo,
			IsActive,
			EntityId,
			CreatedOn
		) 
	Values 
		(
			@FirstName,
			@LastName,
			@Email, 
			'',
			EncryptByPassPhrase('VovTru',CONVERT(varbinary(500),@Pwd)), 
			'ChannelPartner',
			@MobileNo,
			'true',
			(
	SELECT TOP 
		1 BrokerId 
	from 
		ChannelPartnerMaster as CP
	Where
			 CP.Email = @Email And
			 CP.FirstName = @FirstName And 
			 CP.LastName = @LastName And 
			 CP.MobileNo = @MobileNo
	Order By 
		BrokerId 
	DESC
			),
			GETDATE()
		) 
END

	Select
		(FirstName+' '+LastName) As Name,
		UserId 
	From UserMaster
	Where 
		UserId=@Email
		AND FirstName=@FirstName

END
GO

