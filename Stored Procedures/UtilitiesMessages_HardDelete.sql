/****** Object:  StoredProcedure [dbo].[UtilitiesMessages_HardDelete]    Script Date: 05-07-2018 19:50:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[UtilitiesMessages_HardDelete]
(
@Id int
)
as 
BEGIN
	Delete From 
			UtilitiesMessages
	Where
			Id=@Id
END
GO

