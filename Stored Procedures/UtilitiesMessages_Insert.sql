/****** Object:  StoredProcedure [dbo].[UtilitiesMessages_Insert]    Script Date: 05-07-2018 19:51:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[UtilitiesMessages_Insert]
(
@ProcessType varchar(50)=NULL,
@Body varchar(50)=NULL,
@To Varchar(50)=NULL,
@Cc varchar(50)=NULL,
@Bcc varchar(50)=NULL,
@Subject Varchar(50)=NULL
)
as 
BEGIN
	Insert into 
	UtilitiesMessages
	(
	ProcessType,
	Body,
	[To],
	Cc,
	Bcc,
	CreatedDateTime,
	[Subject]
	)
	Values
	(
	@ProcessType,
	@Body,
	@To,
	@Cc,
	@Bcc,
	Getdate(),
	@Subject
	)
END
GO

