import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/index';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    menuItems: any;
    isActive: boolean = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    constructor(private translate: TranslateService, public router: Router, private sharedService:SharedService) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        this.sharedService.GetSideBarMenus().subscribe((response)=>{
            this.menuItems=response;
          });
        // this.menuItems ={
        //     "menu": 
        //        [
        //           {"name": "HOME", "link": "0", "redirectUrl":"/dashboard", "sub": null},
        //           {"name": "ABOUT US", "link": "1", "sub": 
        //              [
        //                 {"name": "TRU Philosophy","redirectUrl":"/bs-element","link": "0-0", "sub": null},
        //                 {"name": "TRU Methodology","redirectUrl":"","link": "0-1", "sub": null},
        //                 {"name": "TRU Team","redirectUrl":"","link": "0-2", "sub": null}
        //              ]
        //           },
        //           {"name": "OUR PROJECTS", "link": "2", "sub": 
        //              [
        //                 {"name": "Residential","redirectUrl":"","link": "2-0", "sub": null},
        //                 {"name": "Commercial","redirectUrl":"","link": "2-1", "sub": null},
        //                 {"name": "IT Parks","redirectUrl":"","link": "2-2", "sub": null}
        //              ]
        //            },        
        //            {"name": "CUSTOMER", "link": "3","sub": 
        //              [
        //                 {"name": "View Profile","redirectUrl":"/view-profile","link": "3-1", "sub": null},
        //                 {"name": "Payment Schedule","redirectUrl":"/payment-schedule","link": "3-2", sub: null},
        //                 {"name": "Financial Status","redirectUrl":"","link": "3-3", "sub": null},
        //                 {"name": "Referral Generation","redirectUrl":"","link": "3-4", "sub": null},
        //                 {"name": "Referral Status","redirectUrl":"","link": "3-5", "sub": null},
        //                 {"name": "Project Status","redirectUrl":"","link": "3-6", "sub": null},
        //                 {"name": "Social Community","redirectUrl":"","link": "3-7", "sub": null},
        //                 {"name": "Report Issues/Concerns","redirectUrl":"","link": "3-8", "sub": null}
        //              ]
        //            }
        //        ]
        //  };
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }
}
