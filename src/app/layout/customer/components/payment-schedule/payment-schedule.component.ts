import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
    selector: 'app-payment-schedule',
    templateUrl: './payment-schedule.component.html',
    styleUrls: ['./payment-schedule.component.scss'],
    animations: [routerTransition()]
})
export class PaymentScheduleComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
