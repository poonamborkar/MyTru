import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentScheduleRoutingModule } from './payment-schedule-routing.module';
import { PaymentScheduleComponent } from './payment-schedule.component';
import { PageHeaderModule } from '../../../../shared';
@NgModule({
    imports: [CommonModule,PageHeaderModule, PaymentScheduleRoutingModule],
    declarations: [PaymentScheduleComponent]
})
export class PaymentScheduleModule {}
