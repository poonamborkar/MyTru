import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { OnDestroy, AfterContentInit, AfterViewInit, AfterViewChecked, OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'tru-flat-details',
  templateUrl: './flat-details.component.html',
  styleUrls: ['./flat-details.component.scss']
})
export class FlatDetailsComponent implements OnChanges {
  @Input() projectFlatDetails: any;
  @Input() index: number;
  public custModel: any = {};
  public Longitude: any;
  public Latitude:any;
  public isCollapsed = false;
  public opened: Boolean = false;
  constructor() { }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.projectFlatDetails && this.projectFlatDetails) {
      this.custModel.custProject = this.projectFlatDetails.ProjectName;
      this.custModel.custBHK = this.projectFlatDetails.BHK;
      this.custModel.custTower = this.projectFlatDetails.Tower;
      this.custModel.custType = this.projectFlatDetails.FlatType;
      this.custModel.custFlat = this.projectFlatDetails.FlatNumber;
      this.Longitude = this.projectFlatDetails.Longitude;
      this.Latitude=this.projectFlatDetails.Latitude;
    }
  }
  toggle=()=> {
    this.opened = !this.opened;
  }


}
