import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { CustomerService } from '../../../../shared/index';

@Component({
    selector: 'app-view-profile',
    templateUrl: './view-profile.component.html',
    styleUrls: ['./view-profile.component.scss'],
    animations: [routerTransition()]
})
export class ViewProfileComponent implements OnInit {
    public custModel: any = {};
    private customer_Info: any = {};
    public flatDetails: any;
    public isEdit: boolean = false;
    public emailPattren: any = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    constructor(private customerService: CustomerService) { }

    ngOnInit() {
        this.customerService.GetCustomerDetails().subscribe((response) => {
            this.BindCustomerModule(response["data"]);
        });
    }
    BindCustomerModule = (customer: any): any => {
        this.custModel.custId = customer[0][0].CustomerId;
        this.custModel.custFirstName = customer[0][0].FirstName;
        this.custModel.custLastName = customer[0][0].LastName;
        this.custModel.custTelephoneNo = customer[0][0].MobileNo;
        this.custModel.custAlternativeTelephoneNo = customer[0][0].Landline;
        this.custModel.custEmail = customer[0][0].Email;
        this.custModel.custAddressLine1 = customer[0][0].AddressLine1;
        this.custModel.custAddressLine2 = customer[0][0].AddressLine2;
        this.custModel.custCity = customer[0][0].City;
        this.custModel.custRegion = customer[0][0].Area;
        this.custModel.custCountry = customer[0][0].Country;
        this.custModel.custPincode = customer[0][0].Pincode;
        this.flatDetails = customer[1];
    }
    OnEditBtnClick = () => {
        this.isEdit = true;
    }
    OnUpdateBtnClick = () => {
        console.log(this.custModel);
        this.customerService.UpdateCustomerDetails(this.custModel).subscribe((response)=>{
            console.log(response);
        });
        this.isEdit = false;
    }
}
