import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewProfileRoutingModule } from './view-profile-routing.module';
import { ViewProfileComponent } from './view-profile.component';
import { PageHeaderModule } from '../../../../shared';
import { GoogleMapComponent } from "./google-map/google-map.component";
import { AgmCoreModule } from '@agm/core';
import { FlatDetailsComponent } from './flat-details/flat-details.component';

@NgModule({
    imports: [CommonModule,
        PageHeaderModule,
        FormsModule,
        ViewProfileRoutingModule,
        AgmCoreModule.forRoot({
            // please get your own API key here:
            // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
            apiKey: 'AIzaSyCZUoS8b99qjiT48T0FMTOyJS07HQ4BdKw'//'AIzaSyByKaxPjMeKQy0JKZgzp8bvkft42u3Wqus'
        })
    ],
    declarations: [ViewProfileComponent,GoogleMapComponent, FlatDetailsComponent]
})
export class ViewProfileModule { }
