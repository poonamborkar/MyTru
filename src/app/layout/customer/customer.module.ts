import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { PageHeaderModule } from '../../shared';
import { ViewProfileModule } from "./components/view-profile/view-profile.module";
@NgModule({
    imports: [CommonModule, CustomerRoutingModule, PageHeaderModule,ViewProfileModule],
    declarations: [CustomerComponent]
})
export class CustomerModule {}
