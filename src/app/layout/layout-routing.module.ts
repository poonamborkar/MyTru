import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'customer', loadChildren: './customer/customer.module#CustomerModule' },
            { path: 'view-profile', loadChildren: './customer/components/view-profile/view-profile.module#ViewProfileModule' },
            { path: 'payment-schedule', loadChildren: './customer/components/payment-schedule/payment-schedule.module#PaymentScheduleModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
