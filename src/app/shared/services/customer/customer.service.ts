import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
// import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { AppConfig } from '../../../app.config';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class CustomerService {


  constructor(
    private http: Http,
    private config: AppConfig
  ) {
    this.http = http;
  }

  GetCustomerDetails = () => {
    const url = this.config.apiUrl + `api/Customer/1`;
    return this.http.get(url).map(
      x => x.json()
    );
  }

  UpdateCustomerDetails = (custModel) => {
    let data = {
      //title: custModel.title,
      //userId: custModel.userId,
      firstName: custModel.custFirstName,
      lastName: custModel.custLastName,
      addressLine1: custModel.custAddressLine1,
      addressLine2: custModel.custAddressLine2,
      //landmark: custModel.landmark,
      area: custModel.custRegion,
      city: custModel.custCity,
      //state: custModel.state,
      country: custModel.custCountry,
      pincode: custModel.custPincode,
      mobileNo: custModel.custTelephoneNo,
      landline: custModel.custAlternativeTelephoneNo,
      email: custModel.custEmail//,
      //preferredCommunication: custModel.preferredCommunication,
      //dateOfBirth: custModel.dateOfBirth
    }
    let body = JSON.stringify(data);
    return this.http.put(this.config.apiUrl + `api/Customer`, body, this.jwt()).map(
      x => x.json()
    );;
  }
  private jwt() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });

  }
}
