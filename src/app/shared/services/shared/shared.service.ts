import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class SharedService {

  constructor(
    private http: Http
  ) {
    this.http = http;
  }

  GetSideBarMenus = () => {
    const url = '../../../../assets/sideMenu.json';
    return this.http.get(url).map(
      x => x.json()
    );
  }
}
